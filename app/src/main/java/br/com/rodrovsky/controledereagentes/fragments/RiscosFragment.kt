package br.com.rodrovsky.controledereagentes.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.ReagenteActivity
import br.com.rodrovsky.controledereagentes.adapters.PictogramaAdapter
import br.com.rodrovsky.controledereagentes.adapters.TextoAdapter
import br.com.rodrovsky.controledereagentes.models.*
import br.com.rodrovsky.controledereagentes.util.classes
import kotlinx.android.synthetic.main.fragment_riscos.*

class RiscosFragment : Fragment() {
    val classesReagente = mutableListOf<Classe>()
    val subclassesReagente = mutableListOf<Subclasse>()
    val pictogramasReagente = mutableListOf<Int>()
    var palavraReagente: String? = null
    val frasesReagente = mutableListOf<FraseAdvertencia>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_riscos, container, false)
    }

    override fun onStart() {
        super.onStart()

        // inicializa listas

        classesReagente.clear()
        subclassesReagente.clear()
        pictogramasReagente.clear()
        palavraReagente = null
        frasesReagente.clear()

        val reagenteActivity = requireActivity() as ReagenteActivity

        reagenteActivity.ocultarInserir()

        val riscos = reagenteActivity.getClassesSubclasses(
            reagenteActivity.getProduto(0).id
        )

        if(riscos.size > 0){
            comClassificacao.visibility = LinearLayout.VISIBLE
            semClassificacao.visibility = LinearLayout.INVISIBLE
        }

        for(risco in riscos) {
            val classe = classes.filter { it.id == risco.classeId }.get(0)
            val subclasse = classe.subclasses.filter { it.id == risco.subclasseId }.get(0)

            subclasse.pictograma?.let {
                pictogramasReagente.addAll(it)
            }

            subclasse.palavra?.let {
                when(it){
                    Texto.PERIGO -> {
                        palavraReagente = it
                    }

                    Texto.ATENCAO -> {
                        if(palavraReagente == null)
                            palavraReagente = it
                    }
                }
            }

            if(palavraReagente == null) {
                palavraReagente = "Nenhuma palavra exigida para a classificação"
            }

            frasesReagente.addAll(subclasse.frases)

            classesReagente.add(classe)
            subclassesReagente.add(subclasse)
        }

        // CLASSIFICACAO RECYCLEVIEW
        listaClassificacao.adapter = TextoAdapter(getClassificacao(), requireContext())
        val classificacaoLayout = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        listaClassificacao.layoutManager = classificacaoLayout

        // PICTOGRAMA RECYCLEVIEW
        listaPictograma.adapter = PictogramaAdapter(pictogramasReagente.toSet().toList(),requireContext())
        val pictogramaLayout = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        listaPictograma.layoutManager = pictogramaLayout

        // PALAVRA ADVERTENCIA RECYCLEVIEW
        txtPalavraAdvertencia.text = palavraReagente

        // FRASES PERIGO RECYCLEVIEW
        listaPerigo.adapter = TextoAdapter(getFrases(TipoTexto.PERIGO), requireContext())
        val perigoLayout = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        listaPerigo.layoutManager = perigoLayout


        // PRECAUCAO PREVENCAO RECYCLEVIEW
        listaPrevencao.adapter = TextoAdapter(getFrases(TipoTexto.PREVENCAO), requireContext())
        val prevencaoLayout = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        listaPrevencao.layoutManager = prevencaoLayout

        // PREAUCAO RESPOSTA RECYCLEVIEW
        listaResposta.adapter = TextoAdapter(getFrases(TipoTexto.RESPOSTA), requireContext())
        val respostaLayout = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        listaResposta.layoutManager = respostaLayout
    }

    fun getClassificacao() : List<String> {
        val classificacao = mutableListOf<String>()

        for((index, classe) in classesReagente.withIndex()) {
            val textoClassificacao = classe.descricao + " (Categoria " + subclassesReagente[index].descricao + ")"
            classificacao.add(textoClassificacao)
        }

        return classificacao
    }

    fun getFrases(tipo : Int) : List<String> {
        val frasesAdvertencia = frasesReagente.filter { it.tipo == tipo }

        val frases = mutableListOf<String>()

        for(frase in frasesAdvertencia){
            frases.add(frase.texto)
        }

        // se frases H410 e H400 forem usadas juntas descartar H400
        if(frases.indexOf(Texto.H410) != -1 && frases.indexOf(Texto.H400) != -1){
            frases.remove(Texto.H400)
        }

        // se frases H411 e H400 forem usadas juntas descartar H401
        if(frases.indexOf(Texto.H411) != -1 && frases.indexOf(Texto.H401) != -1){
            frases.remove(Texto.H401)
        }

        // se frases H410 e H400 forem usadas juntas descartar H400
        if(frases.indexOf(Texto.H412) != -1 && frases.indexOf(Texto.H402) != -1){
            frases.remove(Texto.H402)
        }

        // se frases H410 e H400 forem usadas juntas descartar H400
        if(frases.indexOf(Texto.H314) != -1 && frases.indexOf(Texto.H318) != -1){
            frases.remove(Texto.H318)
        }

        return frases.toSet().toList()
    }

    fun getPictogramas() {
        val pictogramasValidados = mutableListOf<Int>()

        /* se simbolo do cranio se aplica, o ponto de esclamacao nao pode ser utilizado */

        if(pictogramasReagente.indexOf(Pictograma.CRANIO_OSSOS_CRUZADOS) != -1) {
            pictogramasReagente.remove(Pictograma.PONTO_EXCLAMACAO)
        }

        /* se corrosivo se aplica, nao usar ponto de explamacao para irritacao
        aos olhos e pele (classes 20 e 21) */

        if(pictogramasReagente.indexOf(Pictograma.CORROSAO) != -1){
            classesReagente.filter { it.id == 20 || it.id == 21}.let {
                for(classe in it){
                    val index = classesReagente.indexOf(classe)
                    val subclasse = subclassesReagente.get(index)
                    subclasse.pictograma?.let{
                        if(it.indexOf(Pictograma.PONTO_EXCLAMACAO) != -1){
                            subclassesReagente[index].pictograma?.remove(Pictograma.PONTO_EXCLAMACAO)
                        }
                    }
                }
            }
        }

        /* se perigo a saude for utilizado para sensibilizacao respiratoria (classe 22),
         nao usar ponto de exclamacao para sensibilizacao a pele e irritacao aos
         olhos e pele (classes 20, 21 e 23) */

//        val classe22 = classes.filter{ it.id == 22 }.get(0)
//        val index = classesReagente.indexOf(classe22)
//        if(index != -1 && subclassesReagente[index].pictograma?.indexOf(Pictograma.PERIGOSO_SAUDE) != -1){
//
//        }

    }




}