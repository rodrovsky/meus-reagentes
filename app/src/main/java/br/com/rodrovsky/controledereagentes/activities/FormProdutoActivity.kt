package br.com.rodrovsky.controledereagentes.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.Spinner
import androidx.core.view.children
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.adapters.ClasseAdapter
import br.com.rodrovsky.controledereagentes.adapters.SubclasseAdapter
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Classe
import br.com.rodrovsky.controledereagentes.models.Produto
import br.com.rodrovsky.controledereagentes.models.ProdutoRisco
import br.com.rodrovsky.controledereagentes.models.Subclasse
import br.com.rodrovsky.controledereagentes.util.classes
import br.com.rodrovsky.controledereagentes.util.toEditable
import kotlinx.android.synthetic.main.activity_form_produto.*
import kotlinx.android.synthetic.main.activity_form_produto.txtProdutoCodigo
import kotlinx.android.synthetic.main.activity_form_produto.txtProdutoNome
import kotlinx.android.synthetic.main.activity_form_produto.txtProdutoQtdeMinima
import kotlinx.android.synthetic.main.activity_reagente.*
import kotlinx.android.synthetic.main.item_produto_risco.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.*
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import java.text.DecimalFormat

class FormProdutoActivity : AppCompatActivity() {
    val spnClasses = mutableListOf<Spinner>()
    val spnSubclasses = mutableListOf<Spinner>()
    val df = DecimalFormat("#,###.00")

    companion object {
        val REQUEST_CODIGO_PRODUTO = 201
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_produto)

        btnProdutoCodigo.setOnClickListener {
            val intent = Intent(this,LeitorCodigoActivity::class.java)
            startActivityForResult(intent, REQUEST_CODIGO_PRODUTO)
        }

        btn_add_risco.setOnClickListener {
            if(containerClasses.childCount < 10)
                addProdutoRisco(null)
            else{
                toast("Limite máximo de Classes: 10")
            }
        }

        val paramEditar = intent.extras?.getSerializable("produto")

        if(paramEditar == null) {

            btn_salvar_produto.setOnClickListener {
                if (validarDadosCadastro()) {

                    val nome = txtProdutoNome.text.toString()
                    val codigo = txtProdutoCodigo.text.toString()
                    val qtdeMinima = txtProdutoQtdeMinima.text.toString()
                    val controlado = if(chkProdutoControlado.isChecked) 1 else 0

                    var id: Long = -1L
                    database.use {
                        id = insert(
                            "Produto",
                            "nome" to nome,
                            "codigo" to codigo,
                            "qtde_minima" to df.parse(qtdeMinima).toString(),
                            "controlado" to controlado.toString()
                        )


                    }

                    if (id !== -1L) {
                        SalvarClassesSubclasses(id.toInt())

                        toast("Cadastrado com sucesso")
                        finish()
                    } else {
                        toast("Ocorreu um erro")
                    }

                }
            }
        } else {
            val paramProduto = paramEditar as Produto
            val produto = getProduto(paramProduto.id)

            labelFormProduto.text = "Editar Reagente"

            txtProdutoNome.text = produto.nome.toEditable()
            txtProdutoCodigo.text = produto.codigo?.toEditable()
            txtProdutoQtdeMinima.text = df.format(produto.qtdeMinima).toEditable()
            chkProdutoControlado.isChecked = produto.controlado

            btn_salvar_produto.text = "Salvar Alterações"

            val riscos = getClassesSubclasses(produto.id)

            for(risco in riscos){
                addProdutoRisco(risco)
            }

            btn_salvar_produto.setOnClickListener {
                if(validarDadosCadastro()) {
                    val controlado = if(chkProdutoControlado.isChecked) 1 else 0

                    database.use {
                        update("Produto",
                            "nome" to txtProdutoNome.text.toString(),
                            "codigo" to txtProdutoCodigo.text.toString(),
                            "qtde_minima" to df.parse(txtProdutoQtdeMinima.text.toString()).toString(),
                            "controlado" to controlado.toString()
                        ).whereArgs("id = {id}", "id" to produto.id)
                            .exec()
                    }

                    deleteClassesSubclasses(riscos)
                    SalvarClassesSubclasses(produto.id)

                    toast("Atualizado com sucesso")
                    finish()
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == REQUEST_CODIGO_PRODUTO && data != null){
            txtProdutoCodigo.text = data.getStringExtra("codProduto")!!.toEditable()
        }
    }

    fun validarDadosCadastro() : Boolean{
        if(txtProdutoNome.text.isEmpty()){
            txtProdutoNome.error = "Informe o nome"
            return false
        }

        if(txtProdutoCodigo.text.isEmpty()){
            txtProdutoCodigo.error = "Informe o código"
            return false
        }

        if(txtProdutoQtdeMinima.text.isEmpty()){
            txtProdutoQtdeMinima.error = "Informe a quantidade mínima"
            return false
        }

        val spnErro = validarClasses()

        if(spnErro != null){
            val classeRepetida = spnErro.selectedItem as Classe

            alert(
                "Não é possível selecionar a mesma classe mais de uma vez (" + classeRepetida.descricao + "). "
                + "Corrija a classificação para prosseguir.",
                "Atenção") {
                yesButton { }
            }.show()

            return false
        }


        return true
    }

    fun getProduto(id: Int) : Produto {
        return database.use {
            return@use select("Produto")
                .columns("id","nome","codigo", "qtde_minima", "coalesce(controlado,0)")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        nome: String,
                        codigo: String,
                        qtde_minima: Double,
                        controladoTmp: Int
                        ->

                        val controlado = if(controladoTmp == 1) true else false

                        Produto(id, nome, codigo, qtde_minima, controlado)
                    }

                    val produto = parseSingle(parser)

                    return@exec produto
                }
        }
    }


    fun addProdutoRisco(risco: ProdutoRisco?){
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_produto_risco, null)

        val spnClasse = view.findViewById<Spinner>(R.id.spnClasse)
        this.spnClasses.add(spnClasse)
        val classeAdapter = ClasseAdapter(this)
        spnClasse.adapter = classeAdapter
        classeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        classeAdapter.addAll(classes)

        val spnSubclasse = view.findViewById<Spinner>(R.id.spnSubclasse)
        this.spnSubclasses.add(spnSubclasse)
        val subclasseAdapter = SubclasseAdapter(this)
        spnSubclasse.adapter = subclasseAdapter
        subclasseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        subclasseAdapter.addAll(classes.get(0).subclasses)

        spnClasse.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val classe = spnClasse.selectedItem as Classe
                subclasseAdapter.clear()
                subclasseAdapter.addAll(classe.subclasses)
                classeAdapter.notifyDataSetChanged()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        val btnDelete = view.findViewById<Button>(R.id.btn_delete_produto_risco)

        btnDelete.setOnClickListener {
            deleteProdutoRisco(it)
        }

        // carrega classes e subclasses na edicao do reagente
        if(risco != null){
            val classeSelecionada = classes.filter { it.id == risco.classeId }.get(0)
            val classePos = classeAdapter.getPosition(classeSelecionada)
            spnClasse.setSelection(classePos)

            subclasseAdapter.clear()
            subclasseAdapter.addAll(classeSelecionada.subclasses)

            val subclasseSelecionada = classeSelecionada.subclasses.filter { it.id == risco.subclasseId}.get(0)
            val subclassePos = subclasseAdapter.getPosition(subclasseSelecionada)
            spnSubclasse.setSelection(subclassePos)
        }

        containerClasses.addView(view)
    }

    fun deleteProdutoRisco(view: View){
        val index = containerClasses.indexOfChild(view.parent as View)
        Log.d("index", index.toString())
        spnClasses.removeAt(index)
        spnSubclasses.removeAt(index)

        containerClasses.removeView(view.parent as View)
    }

    fun validarClasses() : Spinner? {
        for(spn in spnClasses){
            var contaRepeticoes = 0
            val classe = spn.selectedItem as Classe

            for (spn2 in spnClasses){
                val classe2 = spn2.selectedItem as Classe

                if(classe.id == classe2.id){
                    contaRepeticoes++
                }

                if(contaRepeticoes > 1)
                    return spn2
            }
        }

        return null
    }

    fun SalvarClassesSubclasses(produtoId: Int){
        database.use {
            for ((i, spn) in spnClasses.withIndex()){
                val classe = spn.selectedItem as Classe
                val subclasse = spnSubclasses.get(i).selectedItem as Subclasse

                insert("ProdutoRisco",
                        "produto_id" to produtoId,
                        "classe_id" to classe.id,
                        "subclasse_id" to subclasse.id
                    )
            }
        }
    }

    fun getClassesSubclasses(produtoId: Int) : List<ProdutoRisco> {
        return database.use {
            return@use select("ProdutoRisco")
                .columns("id", "classe_id", "subclasse_id")
                .whereArgs("produto_id = {id}", "id" to produtoId)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        classe_id: Int,
                        subclasse_id : Int ->
                        Log.d("riscos","classe_id: " + classe_id + " - subclasse_id: " + subclasse_id)
                        ProdutoRisco(id, classe_id, subclasse_id)
                    }

                    val riscos = parseList(parser)

                    return@exec riscos
                }
        }
    }

    fun deleteClassesSubclasses(riscos : List<ProdutoRisco>){
        database.use {
            for(risco in riscos){
                delete("ProdutoRisco", "id = {id}","id" to risco.id)
            }
        }
    }
}