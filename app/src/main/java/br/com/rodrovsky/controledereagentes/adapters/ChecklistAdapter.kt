package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.Checklist
import br.com.rodrovsky.controledereagentes.models.Classe
import kotlinx.android.synthetic.main.item_lista_checklist.view.*

class ChecklistAdapter(context: Context) : ArrayAdapter<Checklist>(context,0) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View

        if(convertView != null){
            v = convertView
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.item_lista_checklist,parent,false)
        }

        val item = getItem(position)

        val txtData = v.findViewById<TextView>(R.id.txtData)
        val txtSupervisor = v.findViewById<TextView>(R.id.txtSupervisor)

        txtData.text = item?.data
        txtSupervisor.text = item?.supervisor

        return v
    }

}


