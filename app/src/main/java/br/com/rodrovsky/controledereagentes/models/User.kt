package br.com.rodrovsky.controledereagentes.models

import java.io.Serializable

class User (val id: Int, val matricula: String) : Serializable{

    companion object {
        const val ADMINISTRADOR = 1
        const val TECNICO = 2
        const val SUPERVISOR = 3
    }

    var nome: String? = null
    var email: String? = null
    var telefone: String? = null
    var senha: String? = null
    var perfil: Int? = null

    constructor(id: Int, nome: String, matricula: String) : this(id, matricula) {
        this.nome = nome
    }

    constructor(id: Int, nome: String, matricula: String,
        email: String, telefone: String, senha: String,
        perfil: Int?
    ) : this(id, nome, matricula) {
        this.email = email
        this.telefone = telefone
        this.senha = senha
        this.perfil = perfil
    }
}
