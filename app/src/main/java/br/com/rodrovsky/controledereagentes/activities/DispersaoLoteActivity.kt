package br.com.rodrovsky.controledereagentes.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.adapters.DispersaoAdapter
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Dispersao
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import br.com.rodrovsky.controledereagentes.models.Lote
import kotlinx.android.synthetic.main.activity_dispersao_lote.*
import kotlinx.android.synthetic.main.activity_dispersao_lote.txtProdutoNome
import kotlinx.android.synthetic.main.activity_lote_produto.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import java.text.DecimalFormat

class DispersaoLoteActivity : AppCompatActivity() {
    val df = DecimalFormat("#,###.00")
    lateinit var lote: Lote

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispersao_lote)

        this.lote = intent?.extras?.getSerializable("lote") as Lote


        txtProdutoNome.text = this.lote.produto?.nome
        txtFornecedorNome.text = this.lote.fornecedor?.nome
        txtLoteLote.text = this.lote.lote
        txtLoteDataEntrada.text = this.lote.dataEntrada
        txtLoteDataValidade.text = this.lote.dataValidade
        txtLoteQuantidade.text = df.format(this.lote.quantidade)

        fab_add_dispersao.setOnClickListener {
            val intent = Intent(this,FormDispersaoActivity::class.java)
            intent.putExtra("lote",this.lote)
            startActivity(intent)
        }

        val adapter =  DispersaoAdapter(this)
        list_dispersao.adapter = adapter

        registerForContextMenu(list_dispersao)
    }

    override fun onResume() {
        super.onResume()

        val registros = listarDispersao(this.lote)

        val adapter = list_dispersao.adapter as DispersaoAdapter
        adapter.clear()
        adapter.addAll(registros)

        val disperso = registros.sumByDouble {
            it.quantidade
        }

        val restante = lote.quantidade - disperso
        txtLoteQuantidadeAtual.text = df.format(restante)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        menuInflater.inflate(R.menu.list_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val dispersao = list_dispersao.getItemAtPosition(info.position) as Dispersao

        when(item.itemId) {
            R.id.editar_item -> {
                val intent = Intent(this,FormDispersaoActivity::class.java)
                intent.putExtra("dispersao",dispersao)
                startActivity(intent)
            }

            R.id.excluir_item -> {
                alert("excluir registro?","Atenção") {
                    yesButton {
                        excluirDispersao(dispersao.id)
                        estornarQtdeLote(lote, dispersao.quantidade)
                        toast("Excluído com sucesso")

                        onResume()
                    }

                    noButton {

                    }
                }.show()
            }
        }

        return super.onContextItemSelected(item)
    }



    fun listarDispersao(lote: Lote) : List<Dispersao>{
        return database.use {
            return@use select("Dispersao")
                .whereArgs("lote_id = {loteId}", "loteId" to lote.id)
                .orderBy("date(substr(data,7,4) || '-' || substr(data,4,2) || '-' || substr(data,1,2))", SqlOrderDirection.ASC)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            data: String,
                            quantidade_prevista: Double,
                            quantidade: Double ,
                            lote_id: Int ->

                            Dispersao(id,data,quantidade_prevista,quantidade,lote)

                    }

                    val registros = parseList(parser)

                    return@exec registros
                }
        }
    }

    fun excluirDispersao(id: Int){
        database.use {
            delete("Dispersao", "id = {id}", "id" to id)
        }
    }

    fun estornarQtdeLote(lote: Lote, quantidade: Double) {

        val qtdeAtual = lote.quantidadeAtual + quantidade

        database.use {
            update("Lote",
                    "quantidade_atual" to qtdeAtual
                ).whereArgs("id = {id}", "id" to lote.id)
                .exec()
        }
    }
}