package br.com.rodrovsky.controledereagentes.activities

import android.Manifest
import android.content.Intent
import android.graphics.Color
import android.hardware.Camera
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.fragments.ProdutoFragment
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_leitor_codigo.*
import me.dm7.barcodescanner.core.CameraUtils
import me.dm7.barcodescanner.zxing.ZXingScannerView
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest

class LeitorCodigoActivity : AppCompatActivity(),
        EasyPermissions.PermissionCallbacks,
        ZXingScannerView.ResultHandler {

    val PERMISSAO_CAMERA = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leitor_codigo)

        askCameraPermission()
    }

    override fun onResume() {
        super.onResume()

        svScanner.setResultHandler(this)
        svScanner.setBorderColor(Color.RED)
        svScanner.setLaserColor(Color.YELLOW)
        svScanner.setAutoFocus(true)

        startCamera()
    }



    override fun onPause() {
        super.onPause()

        svScanner.stopCamera()

        val camera = CameraUtils.getCameraInstance()
        if(camera != null){
            (camera as Camera).release()
        }
    }

    fun startCamera() {
        if(EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
            svScanner.startCamera()
        }
    }

    private fun askCameraPermission() {
        EasyPermissions.requestPermissions(
            PermissionRequest.Builder(
                this,
                PERMISSAO_CAMERA,
                Manifest.permission.CAMERA
            ).setRationale("A permissão de uso de câmera é necessária para que o aplicativo funcione.")
                .setPositiveButtonText("OK")
                .setNegativeButtonText("Camcelar")
                .build()
        )
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        startCamera()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        askCameraPermission()
    }

    override fun handleResult(result: Result?) {

        val codigo = result!!.text
        var start = 0
        var end = 0
        var codProduto = ""

        if(codigo.length.rem(2) == 0){
            start = codigo.length / 2
            end = codigo.length - 1
            codProduto = codigo.substring(start,end)
        }else{
            start = (codigo.length + 1) / 2
            codProduto = codigo.substring(start)
        }


//        Log.d("codigo produto",codProduto)

//        svScanner.resumeCameraPreview(this)

        val intent = Intent()
        intent.putExtra("codProduto",codProduto)

        setResult(FormProdutoActivity.REQUEST_CODIGO_PRODUTO,intent)

        finish()
    }
}