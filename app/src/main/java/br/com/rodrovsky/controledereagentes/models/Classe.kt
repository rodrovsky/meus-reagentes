package br.com.rodrovsky.controledereagentes.models

data class Classe (val id: Int, val descricao: String) {
    val subclasses = mutableListOf<Subclasse>()

    override fun toString(): String {
        return this.descricao
    }
}