package br.com.rodrovsky.controledereagentes.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.User
import br.com.rodrovsky.controledereagentes.util.toEditable
import kotlinx.android.synthetic.main.activity_form_user.*
import org.jetbrains.anko.db.*
import org.jetbrains.anko.toast

class FormUserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_user)

        intent.extras?.let {
            // alteracao

            val user = it.getSerializable("user") as User

            labelFormUser.text = "Editar Usuário"

            txtUserNome.text = user.nome?.toEditable()
            txtUserEmail.text = user.email?.toEditable()
            txtUserTelefone.text = user.telefone?.toEditable()
            txtUserLogin.text = user.matricula?.toEditable()
            txtUserSenha.text = user.senha?.toEditable()
            txtUserSenha2.text = user.senha?.toEditable()

            val perfils = if(user.id == 1) listOf<String>("Administrador") else listOf<String>("Administrador", "Supervisor", "Técnico")
            val perfilAdapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, perfils)
            spnUserPerfil.adapter = perfilAdapter

            spnUserPerfil.setSelection(getPerfilPos(user.perfil!!))

            btn_salvar_user.text = "Salvar Alterações"

            btn_salvar_user.setOnClickListener {
                if(validarForm()){
                    val nome = txtUserNome.text.toString()
                    val email = txtUserEmail.text.toString()
                    val telefone = txtUserTelefone.text.toString()
                    val login = txtUserLogin.text.toString()
                    val senha = txtUserSenha.text.toString()
                    val perfilId = getPerfilId(spnUserPerfil.selectedItemPosition)

                    database.use {
                        update("Usuario",
                            "nome" to nome,
                            "email" to email,
                            "telefone" to telefone,
                            "matricula" to login,
                            "senha" to senha,
                            "perfil_id" to perfilId.toString()
                        ).whereArgs("id = {id}", "id" to user.id)
                            .exec()

                        toast("Atualizado com sucesso")
                        finish()
                    }
                }
            }

        } ?: run {

            val perfils = if(getNumUsers() > 0) listOf<String>("Administrador", "Supervisor", "Técnico") else listOf<String>("Administrador")
            val perfilAdapter = ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, perfils)
            spnUserPerfil.adapter = perfilAdapter

            // cadastro
            btn_salvar_user.setOnClickListener {
                if(validarForm()){
                    val nome = txtUserNome.text.toString()
                    val email = txtUserEmail.text.toString()
                    val telefone = txtUserTelefone.text.toString()
                    val login = txtUserLogin.text.toString()
                    val senha = txtUserSenha.text.toString()
                    val perfilId = getPerfilId(spnUserPerfil.selectedItemPosition)

                    database.use {
                        val id = insert("Usuario",
                            "nome" to nome,
                            "email" to email,
                            "telefone" to telefone,
                            "matricula" to login,
                            "senha" to senha,
                            "perfil_id" to perfilId.toString()
                        )

                        if(id != -1L){
                            toast("Cadastrado com sucesso")
                            finish()
                        }else{
                            toast("Ocorreu um erro")
                        }
                    }
                }
            }
        }
    }

    fun validarForm() : Boolean {
        if(txtUserNome.text.isEmpty()){
            txtUserNome.error = "Informe o nome"
            return false
        }

        if(txtUserEmail.text.isEmpty()){
            txtUserEmail.error = "Informe o e-mail"
            return false
        }

        if(txtUserTelefone.text.isEmpty()){
            txtUserTelefone.error = "Informe o telefone"
            return false
        }

        if(txtUserLogin.text.isEmpty()){
            txtUserLogin.error = "Informe o login"
            return false
        }

        if(txtUserSenha.text.isEmpty()){
            txtUserSenha.error = "Informe o senha"
            return false
        }

        if(txtUserSenha2.text.isEmpty()){
            txtUserSenha2.error = "confirme a senha"
            return false
        }

        if(!txtUserSenha.text.toString().equals(txtUserSenha2.text.toString())){
            txtUserSenha2.error = "a senha não confere"
            return false
        }

        return true
    }

    fun getNumUsers() : Int{
        return database.use {
            return@use select("Usuario")
                .columns("count(*)")
                .exec {
                    val parser = rowParser {
                            users: Int ->
                        users
                    }

                    val users = parseSingle(parser)

                    return@exec users
                }
        }
    }

    fun getPerfilId(position: Int) : Int {
        return when(position){
            0 -> User.ADMINISTRADOR
            1 -> User.SUPERVISOR
            2 -> User.TECNICO
            else -> 0
        }
    }

    fun getPerfilPos(id: Int) : Int {
        return when(id){
            User.ADMINISTRADOR -> 0
            User.SUPERVISOR -> 1
            User.TECNICO -> 2
            else -> 0
        }
    }
}