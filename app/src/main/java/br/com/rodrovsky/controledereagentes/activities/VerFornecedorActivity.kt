package br.com.rodrovsky.controledereagentes.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import kotlinx.android.synthetic.main.activity_ver_fornecedor.*
import org.jetbrains.anko.db.parseSingle
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select

class VerFornecedorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ver_fornecedor)

        val param = intent?.extras?.getSerializable("fornecedor") as Fornecedor
        val fornecedor = getFornecedor(param.id)

        txtFornecedorNome.text = fornecedor.nome
        txtFornecedorEndereco.text = fornecedor.endereco
        txtFornecedorEmail.text = fornecedor.email
        txtFornecedorTelefone.text = fornecedor.telefone
        txtFornecedorTelefoneEmergencia.text = fornecedor.telefoneEmergencia

    }

    fun getFornecedor(id: Int) : Fornecedor {
        return database.use {
            return@use select("Fornecedor")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        nome: String,
                        endereco: String,
                        email: String,
                        telefone: String,
                        telefone_emergencia: String
                        ->
                        Fornecedor(id, nome, endereco, email, telefone, telefone_emergencia)
                    }

                    val fornecedor = parseSingle(parser)

                    return@exec fornecedor
                }
        }
    }
}