package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.Dispersao
import java.text.DecimalFormat

class DispersaoAdapter(context: Context) : ArrayAdapter<Dispersao>(context, 0) {
    val df = DecimalFormat("#,###.00")

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View

        if(convertView!=null){
            v = convertView
        }else{
            v = LayoutInflater.from(context).inflate(R.layout.item_lista_dispersao,parent,false)
        }

        val item = getItem(position)

        val txtData =  v.findViewById<TextView>(R.id.txtData)
        txtData.text = item?.data

        val txtQtdePrevista =  v.findViewById<TextView>(R.id.txtQtdePrevista)
        txtQtdePrevista.text = df.format(item?.quantidadePrevista)

        val txtQtde =  v.findViewById<TextView>(R.id.txtQtde)
        txtQtde.text = df.format(item?.quantidade)

        return v
    }

}