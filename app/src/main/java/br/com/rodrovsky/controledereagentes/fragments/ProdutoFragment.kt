package br.com.rodrovsky.controledereagentes.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.*
import br.com.rodrovsky.controledereagentes.adapters.ProdutoAdapter
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import br.com.rodrovsky.controledereagentes.models.Produto
import br.com.rodrovsky.controledereagentes.util.toEditable
import kotlinx.android.synthetic.main.activity_form_produto.*
import kotlinx.android.synthetic.main.fragment_fornecedor.*
import kotlinx.android.synthetic.main.fragment_produto.*

class ProdutoFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_produto, container, false)
    }


    override fun onStart() {
        super.onStart()

        val adapter = ProdutoAdapter(requireActivity().applicationContext)
        list_produto.adapter = adapter
        registerForContextMenu(list_produto)

        btnPesquisaProduto.setOnClickListener {
            val intent = Intent(requireContext(),LeitorCodigoActivity::class.java)
            startActivityForResult(intent, FormProdutoActivity.REQUEST_CODIGO_PRODUTO)
        }

        fab_add_produto.setOnClickListener { view ->
            val intent = Intent(requireActivity().applicationContext,
                FormProdutoActivity::class.java)
            startActivity(intent)
        }

        txtPesquisaProduto.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s != null) {
                    pesquisar(s.toString())
                }
            }
        })

        list_produto.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(requireActivity().applicationContext, ReagenteActivity::class.java)
            val produto = adapter.getItem(position)

            intent.putExtra("produto",produto)

            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == FormProdutoActivity.REQUEST_CODIGO_PRODUTO && data != null){
            txtPesquisaProduto.text = data.getStringExtra("codProduto")!!.toEditable()
        }
    }

    override fun onResume() {
        super.onResume()

        pesquisar(txtPesquisaProduto.text.toString())
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        requireActivity().menuInflater.inflate(R.menu.list_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {

        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val produto = list_produto.getItemAtPosition(info.position) as Produto

        when(item.itemId){
            R.id.editar_item -> {
                val intent = Intent(requireActivity().applicationContext, FormProdutoActivity::class.java)
                intent.putExtra("produto",produto)
                startActivity(intent)
            }

            R.id.excluir_item -> {
                (requireActivity() as MainActivity).confirmDeleteProduto(produto.id, this)
            }
        }

        return super.onContextItemSelected(item)
    }

    fun pesquisar(pesquisa: String) {
        val adapter = list_produto.adapter as ProdutoAdapter
        val produtos = (activity as MainActivity).pesquisarProduto(pesquisa)

        adapter.clear()
        adapter.addAll(produtos)

    }
}