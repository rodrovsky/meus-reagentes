package br.com.rodrovsky.controledereagentes.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Checklist
import kotlinx.android.synthetic.main.activity_ver_checklist.*
import org.jetbrains.anko.db.parseSingle
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class VerChecklistActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ver_checklist)

        val param = intent?.extras?.getSerializable("checklist") as Checklist
        val checklist = getChecklist(param.id)

        txtChecklistCodigo.text = checklist.codigo
        txtChecklistData.text = checklist.data
        txtChecklistSupervisor.text = checklist.supervisor

        txtChecklistResposta1.text = if(checklist.resposta1) "Sim" else "Não"
        txtChecklistResposta2.text = if(checklist.resposta2) "Sim" else "Não"
        txtChecklistResposta3.text = if(checklist.resposta3) "Sim" else "Não"
        txtChecklistResposta4.text = if(checklist.resposta4) "Sim" else "Não"
        txtChecklistResposta5.text = if(checklist.resposta5) "Sim" else "Não"

        txtChecklistCodigo.text = if(checklist.comentarios.isEmpty()) "sem comentários" else checklist.comentarios
    }

    fun getChecklist(id: Int) : Checklist {
        return database.use {
            return@use select("Checklist")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        codigo: String,
                        data: String,
                        supervisor: String,
                        resposta1: Int,
                        resposta2: Int,
                        resposta3: Int,
                        resposta4: Int,
                        resposta5: Int,
                        comentarios: String
                        ->
                        Checklist(
                            id, codigo, data, supervisor,
                            if(resposta1 == 1) true else false,
                            if(resposta2 == 1) true else false,
                            if(resposta3 == 1) true else false,
                            if(resposta4 == 1) true else false,
                            if(resposta5 == 1) true else false,
                            comentarios
                        )
                    }

                    val checklist = parseSingle(parser)
                    return@exec checklist
                }
        }
    }
}