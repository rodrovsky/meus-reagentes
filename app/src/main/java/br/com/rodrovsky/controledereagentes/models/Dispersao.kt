package br.com.rodrovsky.controledereagentes.models

import java.io.Serializable

data class Dispersao(val id: Int, val quantidade: Double) : Serializable {
    var data: String? = null
    var quantidadePrevista: Double? = null
    var lote: Lote? = null

    constructor(id: Int, data:String, quantidade: Double) : this(id,quantidade) {
        this.data = data
    }

    constructor(id: Int, data:String, quantidadePrevista: Double, quantidade: Double) : this(id,quantidade) {
        this.quantidadePrevista = quantidadePrevista
        this.data = data
    }

    constructor(id: Int, data:String, quantidadePrevista: Double, quantidade: Double, lote: Lote) : this(id, data,quantidade) {
        this.quantidadePrevista = quantidadePrevista
        this.lote = lote
    }
}

// data class Dispersao(val id: Int, val data:String, val quantidadePrevista: Double, val quantidade: Double, val lote: Lote) : Serializable