package br.com.rodrovsky.controledereagentes.models

class Texto(val texto: String, val tipo: String) {
    companion object {
        /* Palavras de advertencia */
        const val PERIGO = "Perigo"
        const val ATENCAO = "Atenção"

        /* Frases de perigo */
        const val H200 = "Explosivo; instável"
        const val H201 = "Explosivo; perigo de explosão em massa"
        const val H202 = "Explosivo; perigo grave de projeção"
        const val H203 = "Explosivo; perigo de incêndio, deslocamento de ar ou projeções"
        const val H204 = "Perigo de incêndio ou projeções"
        const val H205 = "Perigo de explosão em massa em caso de incêndio"
        const val H220 = "Gás extremamente inflamável"
        const val H221 = "Gás inflamável"
        const val H222 = "Aerossol extremamente inflamável"
        const val H223 = "Aerossol infl amável"
        const val H224 = "Líquido e vapores extremamente inflamáveis"
        const val H225 = "Líquido e vapores altamente inflamáveis"
        const val H226 = "Líquido e vapores inflamáveis"
        const val H227 = "Líquido combustível"
        const val H228 = "Sólido inflamável"
        const val H229 = "Recipiente pressurizado: pode romper se aquecido"
        const val H230 = "Pode reagir explosivamente mesmo na ausência de ar"
        const val H231 = "Pode reagir explosivamente mesmo na ausência de ar em pressão e/ou temperatura elevada(s)"
        const val H240 = "Pode explodir sob ação do calor"
        const val H241 = "Pode explodir ou incendiar sob ação do calor"
        const val H242 = "Pode incendiar sob ação do calor"
        const val H250 = "Inflama-se espontaneamente em contato com o ar"
        const val H251 = "Sujeito a autoaquecimento, pode se infl amar"
        const val H252 = "Sujeito a autoaquecimento em grandes quantidades, pode se inflamar"
        const val H260 = "Em contato com a água desprende gases inflamáveis que podem infl amar-se espontaneamente"
        const val H261 = "Em contato com a água desprende gases inflamáveis"
        const val H270 = "Pode provocar ou agravar um incêndio, oxidante"
        const val H271 = "Pode provocar incêndio ou explosão, muito comburente"
        const val H272 = "Pode agravar um incêndio, comburente"
        const val H280 = "Contém gás sob pressão: pode explodir sob ação do calor"
        const val H281 = "Contém gás refrigerado: pode causar queimaduras ou lesões criogênicas"
        const val H290 = "Pode ser corrosivo para os metais"
        const val H300 = "Fatal se ingerido"
        const val H301 = "Tóxico se ingerido"
        const val H302 = "Nocivo se ingerido"
        const val H303 = "Pode ser nocivo se ingerido"
        const val H304 = "Pode ser fatal se ingerido e penetrar nas vias respiratórias"
        const val H305 = "Pode ser nocivo se ingerido e penetrar nas vias respiratórias"
        const val H310 = "Fatal em contato com a pele"
        const val H311 = "Tóxico em contato com a pele"
        const val H312 = "Nocivo em contato com a pele"
        const val H313 = "Pode ser nocivo em contato com a pele"
        const val H314 = "Provoca queimadura severa à pele e dano aos olhos"
        const val H315 = "Provoca irritação à pele"
        const val H316 = "Provoca irritação moderada à pele"
        const val H317 = "Pode provocar reações alérgicas na pele"
        const val H318 = "Provoca lesões oculares graves"
        const val H319 = "Provoca irritação ocular grave"
        const val H320 = "Provoca irritação ocular"
        const val H330 = "Fatal se inalado"
        const val H331 = "Tóxico se inalado"
        const val H332 = "Nocivo se inalado"
        const val H333 = "Pode ser nocivo se inalado"
        const val H334 = "Quando inalado pode provocar sintomas alérgicos, de asma ou dificuldades respiratórias"
        const val H335 = "Pode provocar irritação das vias respiratórias"
        const val H336 = "Pode provocar sonolência ou vertigem"
        const val H340 = "Pode provocar defeitos genéticos (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H341 = "Suspeito de provocar defeitos genéticos (descrever a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H350 = "Pode provocar câncer (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H351 = "Suspeito de provocar câncer (descrever a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H360 = "Pode prejudicar a fertilidade ou o feto (indicar o efeito específi co, se conhecido) se ... (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H361 = "Suspeita-se que prejudique a fertilidade ou o feto (indicar o efeito específi co, se conhecido) se ... (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H362 = "Pode ser nocivo às crianças alimentadas com leite materno"
        const val H370 = "Provoca danos aos órgãos (indicar todos os órgãos afetados, se conhecidos) se ... (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H371 = "Pode provocar danos aos órgãos (indicar todos os órgãos afetados, se conhecidos) se ... (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H372 = "Provoca danos aos órgãos (indicar todos os órgãos afetados, se conhecidos) por exposição repetida ou prolongada (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H373 = "Pode provocar danos aos órgãos (indicar todos os órgãos afetados, se conhecidos) por exposição repetida ou prolongada (indicar a via de exposição, se for conclusivamente comprovado que nenhuma outra via de exposição provoca o dano)"
        const val H400 = "Muito tóxico para os organismos aquáticos"
        const val H401 = "Tóxico para os organismos aquáticos"
        const val H402 = "Nocivo para os organismos aquáticos"
        const val H410 = "Muito tóxico para os organismos aquáticos, com efeitos prolongados"
        const val H411 = "Tóxico para os organismos aquáticos, com efeitos prolongados"
        const val H412 = "Nocivo para os organismos aquáticos, com efeitos prolongados"
        const val H413 = "Pode provocar efeitos nocivos prolongados para os organismos aquáticos"
        const val H420 = "Provoca danos à saúde pública e ao meio ambiente pela destruição da camada de ozônio"

        /* Frases de precaucao geral */
        const val P101 = "Se for necessário consultar um médico, tenha em mãos a embalagem ou o rótulo."
        const val P102 = "Mantenha fora do alcance das crianças."
        const val P103 = "Leia o rótulo antes de utilizar o produto."

        /* Frases de precaucao - prevencao */
        const val P201 = "Obtenha instruções específi cas antes da utilização."
        const val P202 = "Não manuseie o produto antes de ter lido e compreendido todas as precauções de segurança."
        const val P210 = "Mantenha afastado do calor/faísca/chama aberta/superfícies quentes. – Não fume."
        const val P211 = "Não pulverize sobre chama aberta ou outra fonte de ignição."
        const val P220 = "Mantenha/guarde afastado de roupa/.../materiais combustíveis."
        const val P221 = "Tome todas as precauções para não misturar com materiais combustíveis..."
        const val P222 = "Não deixe entrar em contato com o ar."
        const val P223 = "Não deixe entrar em contato com água."
        const val P230 = "Mantenha úmido com..."
        const val P231 = "Manuseie em atmosfera de gás inerte."
        const val P232 = "Proteja da umidade."
        const val P233 = "Mantenha o recipiente hermeticamente fechado."
        const val P234 = "Conserve somente no recipiente original."
        const val P235 = "Mantenha em local fresco."
        const val P240 = "Aterre o vaso contentor e o receptor do produto durante transferências."
        const val P241 = "Utilize equipamento elétrico/de ventilação/de iluminação/.../à prova de explosão."
        const val P242 = "Utilize apenas ferramentas antifaiscantes."
        const val P243 = "Evite o acúmulo de cargas eletrostáticas."
        const val P244 = "Mantenha válvulas e conexões isentas de óleos e graxas."
        const val P250 = "Não submeta à abrasão/choque/.../fricção."
        const val P251 = "Não perfure ou queime, mesmo após o uso."
        const val P260 = "Não inale as poeiras/fumos/gases/névoas/vapores/aerossóis."
        const val P261 = "Evite inalar as poeiras/fumos/gases/névoas/vapores/aerossóis."
        const val P262 = "Evite o contato com os olhos, a pele ou a roupa."
        const val P263 = "Evite o contato durante a gravidez/amamentação."
        const val P264 = "Lave cuidadosamente após o manuseio."
        const val P270 = "Não coma, beba ou fume durante a utilização deste produto."
        const val P271 = "Utilize apenas ao ar livre ou em locais bem ventilados."
        const val P272 = "A roupa de trabalho contaminada não pode sair do local de trabalho."
        const val P273 = "Evite a liberação para o meio ambiente."
        const val P280 = "Use luvas de proteção/roupa de proteção/proteção ocular/proteção facial."
        const val P282 = "Use luvas de proteção contra o frio/proteção facial/proteção ocular."
        const val P283 = "Use roupa resistente a/retardadora de fogo/chama."
        const val P284 = "[Em caso de ventilação inadequada] Use equipamento de proteção respiratória."

        /* Frases de precaucao - resposta a emergencia */
        const val P301 = "EM CASO DE INGESTÃO:"
        const val P302 = "EM CASO DE CONTATO COM A PELE:"
        const val P303 = "EM CASO DE CONTATO COM A PELE (ou o cabelo):"
        const val P304 = "EM CASO DE INALAÇÃO:"
        const val P305 = "EM CASO DE CONTATO COM OS OLHOS:"
        const val P306 = "EM CASO DE CONTATO COM A ROUPA:"
        const val P307 = "EM CASO DE exposição:"
        const val P308 = "EM CASO DE exposição ou suspeita de exposição:"
        const val P310 = "Contate imediatamente um CENTRO DE INFORMAÇÃO TOXICOLÓGICA ou um médico."
        const val P311 = "Contate um CENTRO DE INFORMAÇÃO TOXICOLÓGICA/médico."
        const val P312 = "Caso sinta indisposição, contate um CENTRO DE INFORMAÇÃO TOXICOLÓGICA/médico."
        const val P313 = "Consulte um médico."
        const val P314 = "Em caso de mal-estar, consulte um médico."
        const val P315 = "Consulte imediatamente um médico."
        const val P320 = "É urgente um tratamento específi co (veja... neste rótulo)."
        const val P321 = "Tratamento específi co (veja... neste rótulo)."
        const val P330 = "Enxágue a boca."
        const val P331 = "NÃO provoque vômito."
        const val P332 = "Em caso de irritação cutânea:"
        const val P333 = "Em caso de irritação ou erupção cutânea:"
        const val P334 = "Mergulhe em água fria/aplique compressas úmidas."
        const val P335 = "Remova da pele as partículas soltas."
        const val P336 = "Descongele com água morna as áreas afetadas. Não esfregue a área afetada."
        const val P337 = "Caso a irritação ocular persista:"
        const val P338 = "No caso de uso de lentes de contato, remova-as, se for fácil. Continue enxaguando."
        const val P340 = "Remova a pessoa para local ventilado e a mantenha em repouso numa posição que não difi culte a respiração."
        const val P342 = "Em caso de sintomas respiratórios:"
        const val P351 = "Enxágue cuidadosamente com água durante vários minutos."
        const val P352 = "Lave com água em abundância."
        const val P353 = "Enxágue a pele com água/tome uma ducha."
        const val P360 = "Enxágue imediatamente com água em abundância a roupa e a pele contaminadas antes de se despir."
        const val P361 = "Retire imediatamente toda a roupa contaminada."
        const val P362 = "Retire a roupa contaminada."
        const val P363 = "Lave a roupa contaminada antes de usá-la novamente."
        const val P364 = "Lave-a antes de usá-la novamente."
        const val P370 = "Em caso de incêndio:"
        const val P371 = "Em caso de incêndio de grandes proporções:"
        const val P372 = "Risco de explosão em caso de incêndio."
        const val P373 = "NÃO combata o fogo quando ele atingir explosivos."
        const val P374 = "Combata o incêndio tomando as precauções normais, a uma distância razoável."
        const val P375 = "Combata o incêndio à distância, devido ao risco de explosão."
        const val P376 = "Contenha o vazamento se puder ser feito com segurança."
        const val P377 = "Vazamento de gás com chamas: não apague, a menos que se possa conter o vazamento com segurança."
        const val P378 = "Para a extinção utilize"
        const val P380 = "Abandone a área."
        const val P381 = "Elimine todas as fontes de ignição se puder ser feito com segurança."
        const val P390 = "Absorva o produto derramado a fi m de evitar danos materiais."
        const val P391 = "Recolha o material derramado."

        /* Frases de precaucao - armazenamento */
        const val P401 = "Armazene..."
        const val P402 = "Armazene em local seco."
        const val P403 = "Armazene em local bem ventilado."
        const val P404 = "Armazene em recipiente fechado."
        const val P405 = "Armazene em local fechado à chave."
        const val P406 = "Armazene num recipiente resistente à corrosão/... com um revestimento interno resistente."
        const val P407 = "Respeite as distâncias mínimas entre pilhas/paletes."
        const val P410 = "Mantenha ao abrigo da luz solar."
        const val P411 = "Armazene a uma temperatura não superior a... °C."
        const val P412 = "Não exponha a temperaturas superiores a 50 °C."
        const val P413 = "Armazene quantidades a granel superiores a... kg a uma temperatura não superior a... °C."
        const val P420 = "Armazene afastado de outros materiais."
        const val P422 = "Armazene o conteúdo em..."
        
        /* Frases de precaucao - disposicao */
        const val P501 = "Descarte o conteúdo/recipiente em ..."
        const val P502 = "Solicite informações ao fabricante/fornecedor sobre a recuperação/reciclagem."
    }
}