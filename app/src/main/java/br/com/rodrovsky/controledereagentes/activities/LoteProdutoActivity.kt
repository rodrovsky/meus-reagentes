package br.com.rodrovsky.controledereagentes.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.adapters.LoteAdapter
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Dispersao
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import br.com.rodrovsky.controledereagentes.models.Lote
import br.com.rodrovsky.controledereagentes.models.Produto
import kotlinx.android.synthetic.main.activity_lote_produto.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class LoteProdutoActivity : AppCompatActivity() {
    private lateinit var produto: Produto

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lote_produto)

        val param = intent?.extras?.getSerializable("produto") as Produto
        this.produto = getProduto(param.id)

        txtProdutoNome.text = produto.nome
        txtProdutoCodigo.text = produto.codigo
        txtProdutoQtdeMinima.text = produto.qtdeMinima.toString()

        fab_add_lote.setOnClickListener {
            val intent = Intent(this,FormLoteActivity::class.java)
            intent.putExtra("produto",produto)
            startActivity(intent)
        }

        val adapter = LoteAdapter(this)
        list_lote.adapter = adapter
        registerForContextMenu(list_lote)

        list_lote.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this,DispersaoLoteActivity::class.java)

            val lote = adapter.getItem(position)
            intent.putExtra("lote",lote)

            startActivity(intent)


        }
    }


    override fun onResume() {
        super.onResume()

        val adapter = list_lote.adapter as LoteAdapter
        val lotes = listaLote(this.produto.id)

        adapter.clear()
        adapter.addAll(lotes)


        val total = lotes.sumByDouble { it.quantidadeAtual }
        txtProdutoQtdeTotal.text = total.toString()

        if(total <= produto.qtdeMinima){
            txtProdutoNome.setTextColor(Color.parseColor("#ff0000"))
            lblQtdeTotal.setTextColor(Color.parseColor("#ff0000"))
            txtProdutoQtdeTotal.setTextColor(Color.parseColor("#ff0000"))
        } else {
            txtProdutoNome.setTextColor(Color.parseColor("#808080"))
            lblQtdeTotal.setTextColor(Color.parseColor("#808080"))
            txtProdutoQtdeTotal.setTextColor(Color.parseColor("#808080"))
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        menuInflater.inflate(R.menu.list_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val itemLote = list_lote.getItemAtPosition(info.position) as Lote
        val lote = getLote(itemLote.id)

        when(item.itemId) {
            R.id.editar_item -> {
                val intent = Intent(this,FormLoteActivity::class.java)
                intent.putExtra("lote",lote)
                startActivity(intent)
            }

            R.id.excluir_item -> {
                val dispersoes = listaDispersao(lote)

                // nao permitir exclusao se houverem dispersaoes cadastradas para o lote
                if(dispersoes.size > 0){
                    alert(
                        "Não é possível excluir o lote selecionado pois o mesmo possui registros de dispersão",
                        "Atenção") {
                        yesButton { }
                    }.show()

                } else {

                    alert("excluir lote?", "Atenção") {
                        yesButton {
                            excluirLote(lote.id)
                            toast("Excluído com sucesso")

                            onResume()
                        }

                        noButton {

                        }
                    }.show()
                }
            }
        }



        return super.onContextItemSelected(item)

    }

    fun getLote(id: Int) : Lote {
        return database.use {
            return@use select("Lote")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        lote: String,
                        data_entrada: String,
                        data_validade: String,
                        quantidade: Double,
                        quantidade_atual: Double,
                        produto_id: Int,
                        fornecedor_id: Int ->

                        Lote(id,lote,data_entrada, data_validade, quantidade, quantidade_atual,
                        getProduto(produto_id),
                            getFornecedor(fornecedor_id)
                            )
                    }

                    val lote = parseSingle(parser)

                    return@exec lote
                }
        }
    }

    fun getProduto(id: Int) : Produto {
        return database.use {
            return@use select("Produto")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            nome: String,
                            codigo: String,
                            qtde_minima: Double ->

                            Produto(id,nome,codigo,qtde_minima)

                    }

                    val produto = parseSingle(parser)

                    return@exec produto
                }
        }
    }

    fun getFornecedor(id: Int) : Fornecedor {
        return database.use {
            return@use select("Fornecedor")
                .columns("id","nome")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            nome: String ->

                        Fornecedor(id,nome)

                    }

                    val fornecedor = parseSingle(parser)

                    return@exec fornecedor
                }
        }
    }

    fun listaDispersao(lote: Lote) : List<Dispersao>{
        return database.use {
            return@use select("Dispersao")
                .columns("id", "quantidade")
                .whereArgs("lote_id = {loteId}", "loteId" to lote.id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            quantidade: Double ->
                        Dispersao(id,quantidade)

                    }

                    val registros = parseList(parser)

                    return@exec registros
                }
        }
    }

    fun listaLote(produtoId: Int) : List<Lote> {
        return database.use {
            return@use select("Lote")
                .whereArgs("produto_id = {produtoId}", "produtoId" to produtoId)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            lote: String,
                            data_entrada: String,
                            data_validade: String,
                            quantidade: Double,
                            quantidade_atual: Double,
                            produto_id: Int,
                            fornecedor_id: Int ->

                            Lote(
                                id,lote,data_entrada,data_validade,quantidade,quantidade_atual,
                                getProduto(produto_id),
                                getFornecedor(fornecedor_id)
                            )

                    }

                    val lotes = parseList(parser)

                    return@exec lotes
                }
        }
    }

    fun excluirLote(id: Int) {
        database.use {
            delete("Lote", "id = {id}", "id" to id)
        }
    }
}