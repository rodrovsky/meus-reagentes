package br.com.rodrovsky.controledereagentes.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import br.com.rodrovsky.controledereagentes.util.toEditable
import kotlinx.android.synthetic.main.activity_form_fornecedor.*
import org.jetbrains.anko.db.*
import org.jetbrains.anko.toast

class FormFornecedorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_fornecedor)

        val paramAlterar = intent?.extras?.getSerializable("fornecedor")

        if(paramAlterar == null) { // cadastrar
            btn_salvar_fornecedor.setOnClickListener {

                if (validarDadosCadastro()) {
                    val nome = txtFornecedorNome.text.toString()
                    val endereco = txtFornecedorEndereco.text.toString()
                    val email = txtFornecedorEmail.text.toString()
                    val telefone = txtFornecedorTelefone.text.toString()
                    val telefoneEmergencia = txtFornecedorTelefoneEmergencia.text.toString()


                    database.use {
                        val id = insert(
                            "Fornecedor",
                            "nome" to nome,
                            "endereco" to endereco,
                            "email" to email,
                            "telefone" to telefone,
                            "telefone_emergencia" to telefoneEmergencia
                        )

                        if (id !== -1L) {
                            // limparCampos()
                            toast("Cadastrado com sucesso")
                            finish()
                        } else {
                            toast("Ocorreu um erro")
                        }
                    }
                }
            }
        } else { // alterar
            val paramFornecedor = paramAlterar as Fornecedor
            val fornecedor = getFornecedor(paramFornecedor.id)

            labelFormFornecedor.text = " Fornecedor"
            txtFornecedorNome.text = fornecedor.nome.toEditable()
            txtFornecedorEndereco.text = fornecedor.endereco?.toEditable()
            txtFornecedorEmail.text = fornecedor.email?.toEditable()
            txtFornecedorTelefone.text = fornecedor.telefone?.toEditable()
            txtFornecedorTelefoneEmergencia.text = fornecedor.telefoneEmergencia?.toEditable()

            btn_salvar_fornecedor.text = "Salvar Alterações"

            btn_salvar_fornecedor.setOnClickListener {
                if(validarDadosCadastro()) {
                    database.use {
                        update(
                            "Fornecedor",
                            "nome" to txtFornecedorNome.text.toString(),
                            "endereco" to txtFornecedorEndereco.text.toString(),
                            "email" to txtFornecedorEmail.text.toString(),
                            "telefone" to txtFornecedorTelefone.text.toString(),
                            "telefone_emergencia" to txtFornecedorTelefoneEmergencia.text.toString()
                        ).whereArgs("id = {id}", "id" to fornecedor.id)
                            .exec()

                        toast("Atualizado com sucesso")
                        finish()
                    }
                }
            }
        }
    }


    fun validarDadosCadastro() : Boolean {
        if(txtFornecedorNome.text.isEmpty()){
            txtFornecedorNome.error = "Informe o nome do fornecedor"

            return false
        }

        return true
    }

    fun getFornecedor(id: Int) : Fornecedor {
        return database.use {
            return@use select("Fornecedor")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        nome: String,
                        endereco: String,
                        email: String,
                        telefone: String,
                        telefone_emergencia: String
                        ->
                        Fornecedor(id, nome, endereco, email, telefone,  telefone_emergencia)
                    }

                    val fornecedor = parseSingle(parser)

                    return@exec fornecedor
                }
        }
    }
}

