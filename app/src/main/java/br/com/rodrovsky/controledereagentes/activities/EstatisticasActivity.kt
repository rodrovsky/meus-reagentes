package br.com.rodrovsky.controledereagentes.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Dispersao
import br.com.rodrovsky.controledereagentes.models.Produto
import com.jjoe64.graphview.DefaultLabelFormatter
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_estatisticas.*
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import java.text.NumberFormat
import java.text.SimpleDateFormat


class EstatisticasActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estatisticas)

        val paramProduto = intent.extras?.getSerializable("produto")
        val produto = paramProduto as Produto

        val consumo = getConsumo(produto.id)

        val consumoDados = mutableListOf<DataPoint>()

        for(dispersao in consumo) {
            val data = SimpleDateFormat("dd/MM/yyyy").parse(dispersao.data)
            consumoDados.add(DataPoint(data,dispersao.quantidade))
        }

        val dadosArray = consumoDados.toTypedArray()

        val series: LineGraphSeries<DataPoint> = LineGraphSeries<DataPoint>(dadosArray)



        graph.addSeries(series)

        graph.gridLabelRenderer.labelFormatter = DefaultLabelFormatter()
    }


    fun getConsumo(produtoId: Int) : List<Dispersao> {
        return database.use {
            return@use select("Dispersao")
                .columns("id", "data","quantidade")
                .whereArgs("lote_id in (select id from Lote where produto_id = {id})","id" to produtoId)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        data: String,
                        qtde: Double
                        ->
                        Dispersao(id, data, qtde)
                    }

                    val consumo = parseList(parser)

                    return@exec consumo
                }
        }
    }

    class DefaultLabelFormatter(xFormat: NumberFormat, yFormat: NumberFormat)
        : com.jjoe64.graphview.DefaultLabelFormatter(xFormat, yFormat){

        override fun formatLabel(value: Double, isValueX: Boolean) : String {
            if (isValueX) {
                // show normal x values
                return "X"
            } else {
                // show currency for y values
                return "Y"
            }
        }
    }

}