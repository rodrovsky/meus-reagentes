package br.com.rodrovsky.controledereagentes.activities

import android.os.Bundle
import android.util.Log
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.fragments.FornecedorFragment
import br.com.rodrovsky.controledereagentes.fragments.ProdutoFragment
import br.com.rodrovsky.controledereagentes.models.*
import com.jjoe64.graphview.series.DataPoint
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import java.text.SimpleDateFormat

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_dashboard,
            R.id.nav_produto,
            R.id.nav_fornecedor,
            R.id.nav_checklist,
            R.id.nav_users,
            R.id.nav_exportar
        ), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        user = intent?.extras?.getSerializable("user") as User

        restringeMenu(user.perfil!!)

        navView.menu.findItem(R.id.nav_encerrar)?.setOnMenuItemClickListener {
            finish()
            true
        }
    }

    fun restringeMenu(perfil: Int){
        if(perfil != User.ADMINISTRADOR){
            val nav = findViewById<NavigationView>(R.id.nav_view)
            val menu = nav.menu
            menu.findItem(R.id.nav_users).setVisible(false)
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun getUser() : User {
        return user
    }

    fun pesquisarFornecedor(pesquisa: String) : List<Fornecedor>{
        val aux = "%" + pesquisa + "%"

        return database.use {
            return@use select("Fornecedor")
                .columns("id","nome")
                .whereArgs("nome like {pesquisa}", "pesquisa" to aux)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        nome: String ->
                        Fornecedor(id, nome)
                    }

                    var fornecedores = parseList(parser)
                    return@exec fornecedores
                }
        }
    }

    fun pesquisarProduto(pesquisa: String) : List<Produto>{
        val aux = "%" + pesquisa + "%"

        return database.use {
            return@use select("Produto")
                .columns("id","nome","codigo","qtde_minima","coalesce(controlado, 0)")
                .whereArgs("nome like {pesquisa} or codigo like {pesquisa}", "pesquisa" to aux)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            nome: String,
                            codigo: String,
                            qtdeMinima: Double,
                            controlado: Int
                        ->

                        val produto = Produto(
                            id, nome, codigo, qtdeMinima, if(controlado == 1) true else false
                        )

                        produto.vencidos = verificaVencidos(id)

                        produto
                    }

                    var produtos = parseList(parser)
                    return@exec produtos
                }
        }
    }

    fun excluirFornecedor(id: Int){
        database.use {
            delete("Fornecedor", "id = {id}", "id" to id)
        }
    }

    fun excluirProduto(id: Int){
        database.use {
            delete("Produto", "id = {id}","id" to id)

            // exclui os riscos vinculados ao reagente
            select("ProdutoRisco")
                .columns("id", "classe_id", "subclasse_id")
                .whereArgs("produto_id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            classe_id: Int,
                            subclasse_id : Int ->
                        ProdutoRisco(id, classe_id, subclasse_id)
                    }

                    val riscos = parseList(parser)

                    for(risco in riscos){
                        delete("ProdutoRisco", "id = {id}","id" to risco.id)
                    }
                }
        }
    }

    fun getLotesPorFornocedor(fornecedorId: Int) : List<Lote> {
        return database.use {
            return@use select("Lote")
                .columns("id","lote")
                .whereArgs("fornecedor_id = {id}","id" to fornecedorId)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        lote: String ->
                        Lote(id,lote)
                    }

                    val lotes = parseList(parser)

                    return@exec lotes
                }
        }
    }

    fun getLotesPorProduto(produtoId: Int) : List<Lote> {
        return database.use {
            return@use select("Lote")
                .columns("id","lote")
                .whereArgs("produto_id = {id}", "id" to produtoId)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        lote: String
                        ->
                        Lote(id,lote)
                    }

                    val lotes = parseList(parser)

                    return@exec lotes
                }
        }
    }

    fun confirmDeleteFornecedor(fornecedorId: Int, fragment: FornecedorFragment) {
        val lotes = getLotesPorFornocedor(fornecedorId)

        if(lotes.size > 0) {
            alert(
                "Não é possível excluir o fornecedor selecionado pois o mesmo possui lotes de reagentes cadastrados",
                "Atenção") {
                yesButton { }
            }.show()
        } else {
            alert("excluir fornecedor?", "Atenção") {
                yesButton {
                    excluirFornecedor(fornecedorId)
                    toast("Excluído com sucesso")
                    fragment.onResume()
                }

                noButton {

                }
            }.show()
        }
    }

    fun confirmDeleteProduto(produtoId: Int, fragment: ProdutoFragment) {
        val lotes = getLotesPorProduto(produtoId)

        if(lotes.size > 0) {
            alert(
                "Não é possível excluir o reagente selecionado pois o mesmo possui lotes cadastrados",
                "Atenção") {
                yesButton { }
            }.show()
        } else {
            alert("excluir reagente?", "Atenção") {
                yesButton {
                    excluirProduto(produtoId)
                    toast("Excluído com sucesso")
                    fragment.onResume()
                }

                noButton {

                }
            }.show()
        }
    }

    fun getFornecedor(id: Int) : Fornecedor {
        return database.use {
            return@use select("Fornecedor")
                .columns("id", "nome")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        nome: String
                            ->
                        Fornecedor(id, nome)
                    }

                    val fornecedor = parseSingle(parser)

                    return@exec fornecedor
                }
        }
    }

    fun getProduto(id: Int) : Produto {
        return database.use {
            return@use select("Produto")
                .columns("id", "nome")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            nome: String
                        ->
                        Produto(id, nome)
                    }

                    val produto = parseSingle(parser)

                    return@exec produto
                }
        }
    }

    fun getConsumo(loteId : Int) : List<Dispersao> {
        return database.use {
            return@use select("Dispersao")
                .columns("id","data","quantidade_prevista","quantidade")
                .whereArgs("lote_id = {id}","id" to loteId)
                .orderBy("date(substr(data,7,4) || '-' || substr(data,4,2) || '-' || substr(data,1,2))", SqlOrderDirection.ASC)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        data: String,
                        qtde_prevista: Double,
                        qtde: Double
                        ->
                        Dispersao(id, data, qtde_prevista, qtde)
                    }

                    val consumo = parseList(parser)

                    return@exec consumo
                }
        }
    }

    fun getDadosExportacao() : List<Lote> {
        return database.use {
            return@use select("Lote")
                .orderBy("id", SqlOrderDirection.ASC)
                .exec {
                    val parser = rowParser {
                        id: Int,
                        lote: String,
                        data_entrada: String,
                        data_validade: String,
                        quantidade: Double,
                        quantidade_atual: Double,
                        produto_id: Int,
                        fornecedor_id: Int
                        ->
                        val a = 0

                        val lote =Lote(
                            id, lote, data_entrada, data_validade,
                            quantidade, quantidade_atual,
                            getProduto(produto_id),
                            getFornecedor(fornecedor_id)
                        )

                        lote.consumo.addAll(getConsumo(produto_id))

                        lote

                    }

                    val lotes = parseList(parser)

                    return@exec lotes
                }



        }
    }

    fun verificaVencidos(produtoId: Int) : Int {
        return database.use {
            return@use select("Lote")
                .columns("count(*)")
                .whereArgs("produto_id = {id} " +
                        "and date(" +
                        "substr(data_validade,7,4) || '-' " +
                        "|| substr(data_validade,4,2) || '-' " +
                        "|| substr(data_validade,1,2)) between date('now') and date('now', '+90 days')","id" to produtoId)
                .exec {
                    val parser = rowParser {
                        vencidos: Int ->
                        vencidos
                    }

                    val vencidos = parseSingle(parser)

                    return@exec vencidos
                }
        }
    }
}