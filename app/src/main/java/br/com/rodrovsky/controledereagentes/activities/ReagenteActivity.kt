package br.com.rodrovsky.controledereagentes.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.fragments.FornecedorFragment
import br.com.rodrovsky.controledereagentes.fragments.LotesFragment
import br.com.rodrovsky.controledereagentes.models.*
import com.jjoe64.graphview.series.DataPoint
import kotlinx.android.synthetic.main.activity_lote_produto.*
import kotlinx.android.synthetic.main.activity_lote_produto.fab_add_lote
import kotlinx.android.synthetic.main.activity_lote_produto.lblQtdeTotal
import kotlinx.android.synthetic.main.activity_lote_produto.txtProdutoCodigo
import kotlinx.android.synthetic.main.activity_lote_produto.txtProdutoNome
import kotlinx.android.synthetic.main.activity_lote_produto.txtProdutoQtdeMinima
import kotlinx.android.synthetic.main.activity_lote_produto.txtProdutoQtdeTotal
import kotlinx.android.synthetic.main.activity_reagente.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class ReagenteActivity : AppCompatActivity() {
    private lateinit var produto: Produto
    val df = DecimalFormat("#,##0.00")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reagente)
        val navView: BottomNavigationView = findViewById(R.id.nav_view_reagente)

        val navController = findNavController(R.id.nav_host_fragment_reagente)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_lotes, R.id.navigation_estatistica, R.id.navigation_riscos
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        val param = intent?.extras?.getSerializable("produto") as Produto
        this.produto = getProduto(param.id)

        txtProdutoNome.text = produto.nome
        txtProdutoCodigo.text = produto.codigo
        txtProdutoQtdeMinima.text = df.format(produto.qtdeMinima)

        fab_add_lote.setOnClickListener {
            val intent = Intent(this,FormLoteActivity::class.java)
            intent.putExtra("produto",produto)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()

        atualizaTotal()
    }

    fun atualizaTotal() {
        val lotes = listaLote(this.produto.id)
        var total = 0.0

        if(lotes.size > 0){
            total = lotes.sumByDouble { it.quantidadeAtual }
        }

        txtProdutoQtdeTotal.text = df.format(total)

        if(total <= produto.qtdeMinima){
            txtAlertaQtde.visibility = View.VISIBLE
            txtProdutoQtdeTotal.setTextColor(Color.RED)
        } else {
            txtProdutoQtdeTotal.setTextColor(Color.parseColor("#808080"))
            txtAlertaQtde.visibility = View.GONE
        }
    }

    fun getLote(id: Int) : Lote {
        return database.use {
            return@use select("Lote")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            lote: String,
                            data_entrada: String,
                            data_validade: String,
                            quantidade: Double,
                            quantidade_atual: Double,
                            produto_id: Int,
                            fornecedor_id: Int ->

                        Lote(id,lote,data_entrada, data_validade, quantidade, quantidade_atual,
                            getProduto(produto_id),
                            getFornecedor(fornecedor_id)
                        )
                    }

                    val lote = parseSingle(parser)

                    return@exec lote
                }
        }
    }

    fun getProduto(id: Int) : Produto {
        if(id != 0){
            return database.use {
                return@use select("Produto")
                    .columns("id", "nome", "codigo", "qtde_minima")
                    .whereArgs("id = {id}", "id" to id)
                    .exec {
                        val parser = rowParser { id: Int,
                                                 nome: String,
                                                 codigo: String,
                                                 qtde_minima: Double ->

                            Produto(id, nome, codigo, qtde_minima)

                        }

                        val produto = parseSingle(parser)

                        return@exec produto
                    }
            }
        }else{
            return this.produto
        }
    }

    fun getFornecedor(id: Int) : Fornecedor {
        return database.use {
            return@use select("Fornecedor")
                .columns("id","nome")
                .whereArgs("id = {id}", "id" to id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            nome: String ->

                        Fornecedor(id,nome)

                    }

                    val fornecedor = parseSingle(parser)

                    return@exec fornecedor
                }
        }
    }

    fun listaDispersao(lote: Lote) : List<Dispersao>{
        return database.use {
            return@use select("Dispersao")
                .columns("id", "quantidade")
                .whereArgs("lote_id = {loteId}", "loteId" to lote.id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            quantidade: Double ->
                        Dispersao(id,quantidade)

                    }

                    val registros = parseList(parser)

                    return@exec registros
                }
        }
    }

    fun listaLote(produtoId: Int) : List<Lote> {
        return database.use {
            return@use select("Lote")
                .whereArgs("produto_id = {produtoId}", "produtoId" to produtoId)
                .orderBy(
                    "case when quantidade_atual > 0 then 1 else 0 end",
                    SqlOrderDirection.DESC
                )
                .orderBy(
                    "date(substr(data_validade,7,4) " +
                            "|| '-' || substr(data_validade,4,2) || '-' " +
                            "|| substr(data_validade,1,2))",
                    SqlOrderDirection.ASC
                )
                .exec {
                    val parser = rowParser {
                            id: Int,
                            lote: String,
                            data_entrada: String,
                            data_validade: String,
                            quantidade: Double,
                            quantidade_atual: Double,
                            produto_id: Int,
                            fornecedor_id: Int ->

                        Lote(
                            id,lote,data_entrada,data_validade,quantidade,quantidade_atual,
                            getProduto(produto_id),
                            getFornecedor(fornecedor_id)
                        )

                    }

                    val lotes = parseList(parser)

                    return@exec lotes
                }
        }
    }

    fun excluirLote(id: Int) {
        database.use {
            delete("Lote", "id = {id}", "id" to id)
        }
    }

    fun confirmDeleteLote(lote: Lote, fragment: LotesFragment) {
        val lotes = listaDispersao(lote)

        if(lotes.size > 0) {
            alert(
                "Não é possível excluir o lote selecionado pois o mesmo possui consumo cadastrado",
                "Atenção") {
                yesButton { }
            }.show()
        } else {
            alert("excluir lote?", "Atenção") {
                yesButton {
                    excluirLote(lote.id)
                    toast("Excluído com sucesso")
                    fragment.onResume()
                }

                noButton {

                }
            }.show()
        }
    }

    fun getClassesSubclasses(produtoId: Int) : List<ProdutoRisco> {
        return database.use {
            return@use select("ProdutoRisco")
                .columns("id", "classe_id", "subclasse_id")
                .whereArgs("produto_id = {id}", "id" to produtoId)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            classe_id: Int,
                            subclasse_id : Int ->
                        ProdutoRisco(id, classe_id, subclasse_id)
                    }

                    val riscos = parseList(parser)

                    return@exec riscos
                }
        }
    }

    fun getConsumo(produtoId: Int) : List<DataPoint> {
        return database.use {
            return@use select("Dispersao")
                .columns("data","sum(quantidade_prevista)","sum(quantidade)")
                .whereArgs("lote_id in (select id from Lote where produto_id = {id})","id" to produtoId)
                .groupBy("data")
                .orderBy("date(substr(data,7,4) || '-' || substr(data,4,2) || '-' || substr(data,1,2))", SqlOrderDirection.ASC)
                .exec {
                    val parser = rowParser {
                            data: String,
                            qtde_prevista: Double,
                            qtde: Double
                        ->

                        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
                        val dispersao = qtde_prevista - qtde

                        DataPoint(dateFormat.parse(data),dispersao)
                    }

                    val consumo = parseList(parser)

                    return@exec consumo
                }
        }
    }

    fun ocultarInserir() {
        if(fab_add_lote.visibility != View.INVISIBLE) {
            fab_add_lote.visibility = View.INVISIBLE
            ftLegenda.visibility = View.INVISIBLE
        }
    }

    fun mostrarInserir() {
        if(fab_add_lote.visibility != View.VISIBLE) {
            fab_add_lote.visibility = View.VISIBLE
            ftLegenda.visibility = View.VISIBLE
        }
    }
}