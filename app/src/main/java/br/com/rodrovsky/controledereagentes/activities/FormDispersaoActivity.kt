package br.com.rodrovsky.controledereagentes.activities

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Dispersao
import br.com.rodrovsky.controledereagentes.models.Lote
import br.com.rodrovsky.controledereagentes.util.DateWatcher
import br.com.rodrovsky.controledereagentes.util.toEditable
import kotlinx.android.synthetic.main.activity_form_dispersao.*
import kotlinx.android.synthetic.main.activity_form_lote.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.update
import org.jetbrains.anko.toast
import java.text.DecimalFormat
import java.util.*

class FormDispersaoActivity : AppCompatActivity() {
    val df = DecimalFormat("#,###.00")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_dispersao)

        // parametro para cadastro - Lote
        val paramCadastro = intent?.extras?.getSerializable("lote")
        val paramAlteracao = intent?.extras?.getSerializable("dispersao")

        val c = Calendar.getInstance()
        val ano = c.get(Calendar.YEAR)
        val mes = c.get(Calendar.MONTH)
        val dia = c.get(Calendar.DAY_OF_MONTH)

        txtDispersaoData.addTextChangedListener(DateWatcher(txtDispersaoData))

        btnDispersaodata.setOnClickListener {
            val datePicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                val strDay = if(day<10) ("0" + day.toString()) else day
                val strMonth = if(month<9) ("0" + (month + 1).toString()) else month + 1
                txtDispersaoData.text = ("" + strDay + "/" + strMonth + "/" + year).toEditable()
            }, ano, mes, dia)

//                desabilitar datas passadas
//                datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000

            datePicker.show()

        }

        if(paramCadastro != null){
            val lote = paramCadastro as Lote

            btn_salvar_dispersao.setOnClickListener {
                if(validarDadosCadastro()){

                    if(lote.quantidadeAtual < df.parse(txtDispersaoQtde.text.toString()).toDouble()){
                        txtDispersaoQtde.error = "A quantidade não pode ser maior que a quantidade atual do lote"
                    } else {

                        val data = txtDispersaoData.text.toString()
                        val qtdePrevista = txtDispersaoQtdePrevista.text.toString()
                        val qtde = df.parse(txtDispersaoQtde.text.toString())

                        val qtdeAtual = lote.quantidadeAtual - qtde.toDouble()

                        database.use {
                            val id = insert(
                                "Dispersao",
                                "data" to data,
                                "quantidade_prevista" to df.parse(qtdePrevista).toString(),
                                "quantidade" to qtde.toString(),
                                "lote_id" to lote.id

                            )

                            if (id !== -1L) {

                                update("Lote", "quantidade_atual" to qtdeAtual.toString())
                                    .whereArgs("id = {id}", "id" to lote.id)
                                    .exec()

                                // limparCampos()
                                toast("Cadastrado com sucesso")
                                finish()
                            } else {
                                toast("Ocorreu um erro")
                            }
                        }
                    }
                }
            }
        }


        if(paramAlteracao != null){
            val dispersao = paramAlteracao as Dispersao

            labelFormDispersao.text = "Editar Consumo"
            txtDispersaoData.text = dispersao.data?.toEditable()
            txtDispersaoQtdePrevista.text = df.format(dispersao.quantidadePrevista).toEditable()
            txtDispersaoQtde.text = df.format(dispersao.quantidade).toEditable()
            btn_salvar_dispersao.text = "Ŝalvar Alterações"

            btn_salvar_dispersao.setOnClickListener {
                if(validarDadosCadastro()){
                    val qtde = df.parse(txtDispersaoQtde.text.toString()).toDouble()

                    if(dispersao.lote!!.quantidadeAtual < qtde){
                        txtDispersaoQtde.error = "A quantidade não pode ser maior que a quantidade atual do lote"
                    } else {

                        database.use {
                            update(
                                "Dispersao",
                                "data" to txtDispersaoData.text.toString(),
                                "quantidade_prevista" to df.parse(txtDispersaoQtdePrevista.text.toString()).toString(),
                                "quantidade" to qtde.toString()
                            ).whereArgs("id = {id}", "id" to dispersao.id)
                                .exec()
                        }

                        atualizarQtdeAtualLote(dispersao.lote!!, dispersao.quantidade, qtde)

                        toast("Atualizado com sucesso")
                        finish()
                    }
                }
            }
        }
    }

    fun validarDadosCadastro() : Boolean {
        if(txtDispersaoData.text.isEmpty()
            || txtDispersaoData.text.contains("D")
            || txtDispersaoData.text.contains("M")
            || txtDispersaoData.text.contains("Y")){
            txtDispersaoData.error = "Informe a data"
            return false
        }

        if(txtDispersaoQtdePrevista.text.isEmpty()){
            txtDispersaoQtdePrevista.error = "Informe a quantidade prevista"
            return false
        }

        if(txtDispersaoQtde.text.isEmpty()){
            txtDispersaoQtde.error = "Informe a quantidade"
            return false
        }

        return true
    }

    fun atualizarQtdeAtualLote(lote: Lote, qtdeAntiga: Double, qtdeNova: Double) {
        val qtdeAtual = (lote.quantidadeAtual + qtdeAntiga) - qtdeNova

        database.use {
            update("Lote",
                    "quantidade_atual" to qtdeAtual
                )
                .whereArgs("id = {id}", "id" to lote.id)
                .exec()
        }
    }
}