package br.com.rodrovsky.controledereagentes.activities

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Checklist
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import br.com.rodrovsky.controledereagentes.util.DateWatcher
import br.com.rodrovsky.controledereagentes.util.toEditable
import kotlinx.android.synthetic.main.activity_form_checklist.*
import kotlinx.android.synthetic.main.activity_form_lote.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.*
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import java.util.*

class FormChecklistActivity : AppCompatActivity() {
    var resposta1: Boolean? = null
    var resposta2: Boolean? = null
    var resposta3: Boolean? = null
    var resposta4: Boolean? = null
    var resposta5: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_checklist)

        val c = Calendar.getInstance()
        val ano = c.get(Calendar.YEAR)
        val mes = c.get(Calendar.MONTH)
        val dia = c.get(Calendar.DAY_OF_MONTH)

        btnChecklistData.setOnClickListener{
            val dpDataEntrada = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                val strDay = if(day<10) ("0" + day.toString()) else day
                val strMonth = if(month<9) ("0" + (month + 1).toString()) else month + 1
                txtChecklistDate.text = ("" + strDay + "/" + strMonth + "/" + year).toEditable()
            }, ano, mes, dia)

            dpDataEntrada.show()
        }

        txtChecklistDate.addTextChangedListener(DateWatcher(txtChecklistDate))


        intent.extras?.let {
            // editar
            val checklist = it.getSerializable("checklist") as Checklist
            labelFormChecklist.text = "Editar Checklist"

            txtChecklistDate.text = checklist.data.toEditable()
            txtChecklistCodigo.text = checklist.codigo.toEditable()
            txtChecklistSupervisor.text = checklist.supervisor.toEditable()
            txtChecklistComentarios.text = checklist.comentarios.toEditable()

            resposta1 = checklist.resposta1
            if(checklist.resposta1)
                rbSim1.isChecked = true
            else
                rbNao1.isChecked = true

            resposta2 = checklist.resposta2
            if(checklist.resposta2)
                rbSim2.isChecked = true
            else
                rbNao2.isChecked = true

            resposta3 = checklist.resposta3
            if(checklist.resposta3)
                rbSim3.isChecked = true
            else
                rbNao3.isChecked = true

            resposta4 = checklist.resposta4
            if(checklist.resposta4)
                rbSim4.isChecked = true
            else
                rbNao4.isChecked = true

            resposta5 = checklist.resposta5
            if(checklist.resposta5)
                rbSim5.isChecked = true
            else
                rbNao5.isChecked = true


            Log.d("resposta 1", resposta1!!.toString())
            Log.d("resposta 2", resposta2!!.toString())
            Log.d("resposta 3", resposta3!!.toString())
            Log.d("resposta 4", resposta4!!.toString())
            Log.d("resposta 5", resposta5!!.toString())

            btn_salvar_checklist.text = "Salvar Alterações"

            btn_salvar_checklist.setOnClickListener {
                if(validarForm()){
                    database.use {
                        update(
                            "Checklist",
                            "data" to txtChecklistDate.text.toString(),
                            "supervisor" to txtChecklistSupervisor.text.toString(),
                            "codigo" to txtChecklistCodigo.text.toString(),
                            "reposta_1" to if(resposta1!!) "1" else "0",
                            "reposta_2" to if(resposta2!!) "1" else "0",
                            "reposta_3" to if(resposta3!!) "1" else "0",
                            "reposta_4" to if(resposta4!!) "1" else "0",
                            "reposta_5" to if(resposta5!!) "1" else "1",
                            "comentarios" to txtChecklistComentarios.text.toString()
                        ).whereArgs("id = {id}", "id" to checklist.id)
                            .exec()

                        Log.d("resposta 1", resposta1!!.toString())
                        Log.d("resposta 2", resposta2!!.toString())
                        Log.d("resposta 3", resposta3!!.toString())
                        Log.d("resposta 4", resposta4!!.toString())
                        Log.d("resposta 5", resposta5!!.toString())

                        toast("Atualizado com sucesso")
                        finish()
                    }
                }
            }


        } ?: run {
            // cadastrar

            btn_salvar_checklist.setOnClickListener {
                if(validarForm()){
                    database.use {
                        val id = insert("Checklist",
                                "data" to txtChecklistDate.text.toString(),
                                "codigo" to txtChecklistCodigo.text.toString(),
                                "supervisor" to txtChecklistSupervisor.text.toString(),
                                "reposta_1" to if(resposta1!!) "1" else "0",
                                "reposta_2" to if(resposta2!!) "1" else "0",
                                "reposta_3" to if(resposta3!!) "1" else "0",
                                "reposta_4" to if(resposta4!!) "1" else "0",
                                "reposta_5" to if(resposta5!!) "1" else "1",
                                "comentarios" to txtChecklistComentarios.text.toString()
                            )


                        if(id !== -1L){
                            // limparCampos()
                            toast("Cadastrado com sucesso")
                            finish()
                        } else{
                            toast("Ocorreu um erro")
                        }
                    }


                }
            }
        }
    }

    fun validarForm() : Boolean {
        if(txtChecklistDate.text.isBlank()){
            txtChecklistDate.error = "Informe a data"
            return false
        }

        if(txtChecklistCodigo.text.isBlank()){
            txtChecklistCodigo.error = "Informe o ID"
            return false
        }

        if(txtChecklistSupervisor.text.isBlank()){
            txtChecklistSupervisor.error = "Informe o nome do supervisor"
            return false
        }

        if(resposta1 == null
            || resposta2 == null
            || resposta3 == null
            || resposta4 == null
            || resposta5 == null){

            alert("Todas as perguntas devem ser respondidas", "Atenção") {
                yesButton {  }
            }.show()

            return false
        }

        return true
    }

    fun onRadioButtonClicked(view: View){
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                // resposta 1
                R.id.rbSim1 -> {
                    Log.d("resposta 1","sim")
                    if (checked)
                        resposta1 = true
                }
                R.id.rbNao1 -> {
                    Log.d("resposta 1","nao")
                    if (checked)
                        resposta1 = false
                }

                // resposta 2
                R.id.rbSim2 -> {
                    Log.d("resposta 2","sim")
                    if (checked)
                        resposta2 = true
                }
                R.id.rbNao2 -> {
                    Log.d("resposta 2","nao")
                    if (checked)
                        resposta2 = false
                }
                // resposta 3
                R.id.rbSim3 -> {
                    Log.d("resposta 3","sim")
                    if (checked)
                        resposta3 = true
                }
                R.id.rbNao3 -> {
                    Log.d("resposta 3","nao")
                    if (checked)
                        resposta3 = false
                }
                // resposta 4
                R.id.rbSim4 -> {
                    Log.d("resposta 4","sim")
                    if (checked)
                        resposta4 = true
                }
                R.id.rbNao4 -> {
                    Log.d("resposta 4","nao")
                    if (checked)
                        resposta4 = false
                }
                // resposta 5
                R.id.rbSim5 -> {
                    Log.d("resposta 5","sim")
                    if (checked)
                        resposta5 = true
                }
                R.id.rbNao5 -> {
                    Log.d("resposta 5","nao")
                    if (checked)
                        resposta5 = false
                }
            }


        }
    }
}