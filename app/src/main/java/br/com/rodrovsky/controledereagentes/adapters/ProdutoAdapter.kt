package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.graphics.Color
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.Produto

class ProdutoAdapter(context: Context) : ArrayAdapter<Produto>(context, 0) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View
        if(convertView != null){
            v = convertView
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.item_lista_produto, parent, false)
        }

        val item = getItem(position)

        if(item != null){
            val txtProduto = v.findViewById<TextView>(R.id.txtProduto)
            val imgControlado = v.findViewById<ImageView>(R.id.imgControlado)
            txtProduto.text = item.nome

            if(item.vencidos > 0){
                txtProduto.setTextColor(Color.parseColor("#FF8607"))
            }

            imgControlado.setColorFilter(Color.parseColor("#000000"))
            if(item.controlado){
                imgControlado.visibility = ImageView.VISIBLE
            } else {
                imgControlado.visibility = ImageView.INVISIBLE
            }
        }

        return v

    }
}