package br.com.rodrovsky.controledereagentes.models

import br.com.rodrovsky.controledereagentes.R

data class Pictograma(val resourceId: Int) {
    companion object {
        const val BOMBA_EXPLODINDO = R.drawable.pictograma_bomba_explodindo_70
        const val CHAMA = R.drawable.pictograma_chama_70
        const val CHAMA_SOBRE_CIRCULO = R.drawable.pictograma_chama_sobre_circulo_70
        const val CILINDRO_GAS = R.drawable.pictograma_cilindro_gas_70
        const val CORROSAO = R.drawable.pictograma_corrosao_70
        const val CRANIO_OSSOS_CRUZADOS = R.drawable.pictograma_cranio_ossos_cruzados_70
        const val MEIO_AMBIENTE = R.drawable.pictograma_meio_ambiente_70
        const val PERIGOSO_SAUDE = R.drawable.pictograma_perigoso_saude_70
        const val PONTO_EXCLAMACAO = R.drawable.pictograma_ponto_exclamacao_70
    }
}