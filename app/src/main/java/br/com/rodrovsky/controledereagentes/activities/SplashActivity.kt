package br.com.rodrovsky.controledereagentes.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.*
import br.com.rodrovsky.controledereagentes.util.classes
import org.jetbrains.anko.*
import org.jetbrains.anko.db.parseSingle
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // segura a splash por 2 segundos
        Handler().postDelayed({
//            initClassesSubclasses()
            initApp()
        }, 2000)
    }

    fun initApp(){
        initClassesSubclasses()
        login()
        finish()
    }


//
//    fun getPrimeiroUsuario() : User{
//        return database.use {
//            return@use select("Usuario")
//                .columns("id","nome","matricula")
//                .exec {
//                    val parser = rowParser {
//                        id: Int,
//                        nome: String,
//                        matricula: String ->
//                        User(id, nome, matricula)
//                    }
//
//                    val user = parseSingle(parser)
//                    return@exec user
//                }
//        }
//    }

    fun login(){
        val intent = Intent(this,LoginActivity::class.java)
        startActivity(intent)
    }

    fun initClassesSubclasses(){
        val classe1 = Classe(1, "Explosivos")

        val subclasse1 = Subclasse(
            1,
            "Explosivos instáveis",
            Texto.PERIGO,
            mutableListOf(Pictograma.BOMBA_EXPLODINDO)
        )

        val frase1 = FraseAdvertencia(Texto.H200, TipoTexto.PERIGO)
        val frase2 = FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO)
        val frase3 = FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO)
        val frase4 = FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO)
        val frase5 = FraseAdvertencia(Texto.P372, TipoTexto.RESPOSTA)
        val frase6 = FraseAdvertencia(Texto.P373, TipoTexto.RESPOSTA)
        val frase7 = FraseAdvertencia(Texto.P380, TipoTexto.RESPOSTA)

        subclasse1.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H200, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P372, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P373, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P380, TipoTexto.RESPOSTA)
            )
        )

        classe1.subclasses.add(subclasse1)

        val subclasse2 = Subclasse(
            2,
            "Divisão 1.1",
            Texto.PERIGO,
            mutableListOf(Pictograma.BOMBA_EXPLODINDO)
        )

        subclasse2.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H201, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P250, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P380, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P372, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P373, TipoTexto.RESPOSTA)
            )
        )

        classe1.subclasses.add(subclasse2)

        val subclasse3 = Subclasse(
            3,
            "Divisão 1.2",
            Texto.PERIGO,
            mutableListOf(Pictograma.BOMBA_EXPLODINDO)
        )

        subclasse3.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H202, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P230, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P250, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P380, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P372, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P373, TipoTexto.RESPOSTA)
            )
        )

        classe1.subclasses.add(subclasse3)

        val subclasse4 = Subclasse(
            4,
            "Divisão 1.3",
            Texto.PERIGO,
            mutableListOf(Pictograma.BOMBA_EXPLODINDO)
        )

        subclasse4.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H203, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P230, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P250, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P380, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P372, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P373, TipoTexto.RESPOSTA)
            )
        )

        classe1.subclasses.add(subclasse4)

        val subclasse5 = Subclasse(
            5,
            "Divisão 1.4",
            Texto.ATENCAO,
            mutableListOf(Pictograma.BOMBA_EXPLODINDO)
        )

        subclasse5.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H204, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P250, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P380, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P372, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P373, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P374, TipoTexto.RESPOSTA)
            )
        )

        classe1.subclasses.add(subclasse5)

        val subclasse6 = Subclasse(6, "Divisão 1.5", Texto.PERIGO, null)

        subclasse6.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H205, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P230, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P250, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P380, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P372, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P373, TipoTexto.RESPOSTA)
            )
        )

        classe1.subclasses.add(subclasse6)

        val subclasse7 = Subclasse(7, "Divisão 1.6", null, null)
        classe1.subclasses.add(subclasse7)

        // sem elementos de rotulagem

        classes.add(classe1)


        val classe2 = Classe(2, "Gases inflamáveis (incluindo os gases quimicamente instáveis)")

        val subclasse8 = Subclasse(8, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse8.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H220, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P377, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P381, TipoTexto.RESPOSTA)
            )
        )

        classe2.subclasses.add(subclasse8)

        val subclasse9 = Subclasse(9, "2", Texto.ATENCAO, null)

        subclasse9.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H221, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P377, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P381, TipoTexto.RESPOSTA)
            )
        )

        classe2.subclasses.add(subclasse9)

        val subclasse10 = Subclasse(10, "Quimicamente instáveis A", null, null)

        subclasse10.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H230, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO)
            )
        )

        classe2.subclasses.add(subclasse10)

        val subclasse11 = Subclasse(11, "Quimicamente instáveis B", null, null)

        subclasse11.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H231, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO)
            )
        )

        classe2.subclasses.add(subclasse11)

        classes.add(classe2)


        val classe3 = Classe(3, "Aerossóis")

        val subclasse12 = Subclasse(12, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse12.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H222, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P211, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P251, TipoTexto.PREVENCAO)
            )
        )

        classe3.subclasses.add(subclasse12)

        val subclasse13 = Subclasse(13, "2", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA))

        subclasse13.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H223, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P211, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P251, TipoTexto.PREVENCAO)
            )
        )

        classe3.subclasses.add(subclasse13)

        val subclasse14 = Subclasse(14, "3", Texto.ATENCAO, null)

        subclasse14.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H229, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P251, TipoTexto.PREVENCAO)
            )
        )

        classe3.subclasses.add(subclasse14)

        classes.add(classe3)


        val classe4 = Classe(4, "Gases oxidantes")

        val subclasse15 = Subclasse(15, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA_SOBRE_CIRCULO))

        subclasse15.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H270, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P244, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P376, TipoTexto.RESPOSTA)
            )
        )

        classe4.subclasses.add(subclasse15)

        classes.add(classe4)


        val classe5 = Classe(5, "Gases sob pressão")

        val subclasse16 = Subclasse(
            16,
            "Gás comprimido",
            Texto.ATENCAO,
            mutableListOf(Pictograma.CILINDRO_GAS)
        )

        subclasse16.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H280, TipoTexto.PERIGO)
            )
        )

        classe5.subclasses.add(subclasse16)

        val subclasse17 = Subclasse(
            17,
            "Gás liquefeito",
            Texto.ATENCAO,
            mutableListOf(Pictograma.CILINDRO_GAS)
        )

        subclasse17.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H280, TipoTexto.PERIGO)
            )
        )

        classe5.subclasses.add(subclasse17)

        val subclasse18 = Subclasse(
            18, "Gás liquefeito refrigerado", Texto.ATENCAO, mutableListOf(
                Pictograma.CILINDRO_GAS
            )
        )

        subclasse18.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H281, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P282, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P336, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P315, TipoTexto.RESPOSTA)
            )
        )

        classe5.subclasses.add(subclasse18)

        val subclasse19 = Subclasse(
            19,
            "Gás dissolvido",
            Texto.ATENCAO,
            mutableListOf(Pictograma.CILINDRO_GAS)
        )

        subclasse19.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H280, TipoTexto.PERIGO)
            )
        )

        classe5.subclasses.add(subclasse19)

        classes.add(classe5)


        val classe6 = Classe(6, "Líquidos inflamáveis")

        val subclasse20 = Subclasse(20, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse20.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H224, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P233, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P241, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P242, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P243, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P303 + " " + Texto.P361 + " " + Texto.P353, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe6.subclasses.add(subclasse20)

        val subclasse21 = Subclasse(21, "2", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse21.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H225, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P233, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P241, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P242, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P243, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P303 + " " + Texto.P361 + " " + Texto.P353, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )


        classe6.subclasses.add(subclasse21)

        val subclasse22 = Subclasse(22, "3", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA))

        subclasse22.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H226, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P233, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P241, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P242, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P243, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P303 + " " + Texto.P361 + " " + Texto.P353, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe6.subclasses.add(subclasse22)

        val subclasse23 = Subclasse(23, "4", Texto.ATENCAO, null)

        subclasse23.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H227, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe6.subclasses.add(subclasse23)

        classes.add(classe6)


        val classe7 = Classe(7, "Sólidos inflamáveis")

        val subclasse24 = Subclasse(24, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse24.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H228, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P241, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe7.subclasses.add(subclasse24)

        val subclasse25 = Subclasse(25, "2", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA))

        subclasse25.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H228, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P240, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P241, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe7.subclasses.add(subclasse25)

        classes.add(classe7)


        val classe8 = Classe(8, "Substâncias e misturas autorreativas")

        val subclasse26 = Subclasse(26, "A", Texto.PERIGO, mutableListOf(Pictograma.BOMBA_EXPLODINDO))

        subclasse26.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H240, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P380 + " " + Texto.P375, TipoTexto.RESPOSTA)
            )
        )

        classe8.subclasses.add(subclasse26)

        val subclasse27 = Subclasse(
            27, "B", Texto.PERIGO, mutableListOf(
                Pictograma.CHAMA,
                Pictograma.BOMBA_EXPLODINDO
            )
        )

        subclasse27.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H241, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P380 + " " + Texto.P375, TipoTexto.RESPOSTA)
            )
        )

        classe8.subclasses.add(subclasse27)

        val subclasse28 = Subclasse(28, "C e D", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse28.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H242, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe8.subclasses.add(subclasse28)

        val subclasse29 = Subclasse(29, "E e F", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA))

        subclasse29.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H242, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe8.subclasses.add(subclasse29)

        val subclasse30 = Subclasse(30, "G", null, null)

        // sem elementos de rotulagem

        classe8.subclasses.add(subclasse30)

        classes.add(classe8)


        val classe9 = Classe(9, "Líquidos pirofóricos")

        val subclasse31 = Subclasse(31, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse31.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H250, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P222, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P302 + " " + Texto.P334, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe9.subclasses.add(subclasse31)

        classes.add(classe9)


        val classe10 = Classe(10, "Sólidos pirofóricos")

        val subclasse32 = Subclasse(32, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse32.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H250, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P222, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P335 + " " + Texto.P334, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe10.subclasses.add(subclasse32)

        classes.add(classe10)


        val classe11 = Classe(11, "Substâncias e misturas sujeitas a autoaquecimento")

        val subclasse33 = Subclasse(33, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse33.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H251, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P235 + " " + Texto.P410, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO)
            )
        )

        classe11.subclasses.add(subclasse33)

        val subclasse34 = Subclasse(34, "2", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA))

        subclasse34.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H252, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P235 + " " + Texto.P410, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO)
            )
        )

        classe11.subclasses.add(subclasse34)

        classes.add(classe11)


        val classe12 = Classe(
            12,
            "Substâncias e misturas que, em contato com a água, emitem gases inflamáveis"
        )

        val subclasse35 = Subclasse(35, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse35.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H260, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P223, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P231 + " " + Texto.P232, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P335 + " " + Texto.P334, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe12.subclasses.add(subclasse35)

        val subclasse36 = Subclasse(36, "2", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse36.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H261, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P223, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P231 + " " + Texto.P232, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P335 + " " + Texto.P334, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe12.subclasses.add(subclasse36)

        val subclasse37 = Subclasse(37, "3", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA))

        subclasse37.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H261, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P231 + " " + Texto.P232, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe12.subclasses.add(subclasse37)

        classes.add(classe12)


        val classe13 = Classe(13, "Líquidos oxidantes")

        val subclasse38 = Subclasse(38, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA_SOBRE_CIRCULO))

        subclasse38.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H271, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P221, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P283, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P306 + " " + Texto.P360, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P371 + " " + Texto.P380 + " " + Texto.P375, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe13.subclasses.add(subclasse38)

        val subclasse39 = Subclasse(39, "2", Texto.PERIGO, mutableListOf(Pictograma.CHAMA_SOBRE_CIRCULO))

        subclasse39.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H272, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P221, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe13.subclasses.add(subclasse39)

        val subclasse40 = Subclasse(40, "3", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA_SOBRE_CIRCULO))

        subclasse40.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H272, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P221, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe13.subclasses.add(subclasse40)

        classes.add(classe13)


        val classe14 = Classe(14, "Sólidos oxidantes")

        val subclasse41 = Subclasse(41, "1", Texto.PERIGO, mutableListOf(Pictograma.CHAMA_SOBRE_CIRCULO))

        subclasse41.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H271, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P221, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P283, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P306 + " " + Texto.P360, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P371 + " " + Texto.P380 + " " + Texto.P375, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe14.subclasses.add(subclasse41)

        val subclasse42 = Subclasse(42, "2", Texto.PERIGO, mutableListOf(Pictograma.CHAMA_SOBRE_CIRCULO))

        subclasse42.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H272, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P221, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe14.subclasses.add(subclasse42)

        val subclasse43 = Subclasse(43, "3", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA_SOBRE_CIRCULO))

        subclasse43.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H272, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P221, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P370 + " " + Texto.P378, TipoTexto.RESPOSTA)
            )
        )

        classe14.subclasses.add(subclasse43)

        classes.add(classe14)


        val classe15 = Classe(15, "Peróxidos orgânicos")

        val subclasse44 = Subclasse(44, "A", Texto.PERIGO, mutableListOf(Pictograma.BOMBA_EXPLODINDO))

        subclasse44.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H240, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO)
            )
        )

        classe15.subclasses.add(subclasse44)

        val subclasse45 = Subclasse(
            45, "B", Texto.PERIGO, mutableListOf(
                Pictograma.CHAMA,
                Pictograma.BOMBA_EXPLODINDO
            )
        )

        subclasse45.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H241, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO)
            )
        )

        classe15.subclasses.add(subclasse45)

        val subclasse46 = Subclasse(46, "C e D", Texto.PERIGO, mutableListOf(Pictograma.CHAMA))

        subclasse46.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H242, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO)
            )
        )

        classe15.subclasses.add(subclasse46)

        val subclasse47 = Subclasse(47, "E e F", Texto.ATENCAO, mutableListOf(Pictograma.CHAMA))

        subclasse47.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H242, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P210, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P220, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO)
            )
        )

        classe15.subclasses.add(subclasse47)

        val subclasse48 = Subclasse(48, "G", null, null)

        // sem elementos de rotulagem

        classe15.subclasses.add(subclasse48)

        classes.add(classe15)


        val classe16 = Classe(16, "Corrosivo para os metais")

        val subclasse49 = Subclasse(49, "1", Texto.ATENCAO, mutableListOf(Pictograma.CORROSAO))

        subclasse49.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H290, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P234, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P390, TipoTexto.PREVENCAO)
            )
        )

        classe16.subclasses.add(subclasse49)

        classes.add(classe16)


        val classe17 = Classe(17, "Toxicidade aguda – Oral")

        val subclasse50 = Subclasse(50, "1", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse50.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H300, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P301 + " " + Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P330, TipoTexto.RESPOSTA)
            )
        )

        classe17.subclasses.add(subclasse50)

        val subclasse51 = Subclasse(51, "2", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse51.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H300, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P301 + " " + Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P330, TipoTexto.RESPOSTA)
            )
        )

        classe17.subclasses.add(subclasse51)

        val subclasse52 = Subclasse(52, "3", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse52.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H301, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P301 + " " + Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P330, TipoTexto.RESPOSTA)
            )
        )

        classe17.subclasses.add(subclasse52)

        val subclasse53 = Subclasse(53, "4", Texto.ATENCAO, mutableListOf(Pictograma.PONTO_EXCLAMACAO))

        subclasse53.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H302, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P301 + " " + Texto.P312, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P330, TipoTexto.RESPOSTA)
            )
        )

        classe17.subclasses.add(subclasse53)

        val subclasse54 = Subclasse(54, "5", Texto.ATENCAO, null)

        subclasse54.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H303, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P312, TipoTexto.RESPOSTA)
            )
        )

        classe17.subclasses.add(subclasse54)


        classes.add(classe17)


        val classe18 = Classe(18, "Toxicidade aguda – Dérmica")

        val subclasse55 = Subclasse(55, "1", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse55.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H310, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P262, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P302 + " " + Texto.P352, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P361 + " " + Texto.P364, TipoTexto.RESPOSTA)
            )
        )

        classe18.subclasses.add(subclasse55)

        val subclasse56 = Subclasse(56, "2", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse56.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H310, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P262, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P302 + " " + Texto.P352, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P361 + " " + Texto.P364, TipoTexto.RESPOSTA)
            )
        )

        classe18.subclasses.add(subclasse56)

        val subclasse57 = Subclasse(57, "3", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse57.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H311, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P302 + " " + Texto.P352, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P312, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P361 + " " + Texto.P364, TipoTexto.RESPOSTA)
            )
        )

        classe18.subclasses.add(subclasse57)

        val subclasse58 = Subclasse(58, "4", Texto.ATENCAO, mutableListOf(Pictograma.PONTO_EXCLAMACAO))

        subclasse58.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H312, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P302 + " " + Texto.P352, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P312, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P361 + " " + Texto.P364, TipoTexto.RESPOSTA)
            )
        )

        classe18.subclasses.add(subclasse58)

        val subclasse59 = Subclasse(59, "5", Texto.ATENCAO, null)

        subclasse59.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H313, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P312, TipoTexto.RESPOSTA)
            )
        )

        classe18.subclasses.add(subclasse59)

        classes.add(classe18)


        val classe19 = Classe(19, "Toxicidade aguda – Inalação")

        val subclasse60 = Subclasse(60, "1", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse60.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H330, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P271, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P284, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P320, TipoTexto.RESPOSTA)
            )
        )

        classe19.subclasses.add(subclasse60)

        val subclasse61 = Subclasse(61, "2", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse61.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H330, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P271, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P284, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P320, TipoTexto.RESPOSTA)
            )
        )

        classe19.subclasses.add(subclasse61)

        val subclasse62 = Subclasse(62, "3", Texto.PERIGO, mutableListOf(Pictograma.CRANIO_OSSOS_CRUZADOS))

        subclasse62.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H331, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P261, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P271, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P311, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA)
            )
        )

        classe19.subclasses.add(subclasse62)

        val subclasse63 = Subclasse(63, "4", Texto.ATENCAO, mutableListOf(Pictograma.PONTO_EXCLAMACAO))

        subclasse63.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H332, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P261, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P271, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P312, TipoTexto.RESPOSTA)
            )
        )

        classe19.subclasses.add(subclasse63)

        val subclasse64 = Subclasse(64, "5", Texto.ATENCAO, null)

        subclasse64.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H333, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P304 + " " + Texto.P312, TipoTexto.RESPOSTA)
            )
        )

        classe19.subclasses.add(subclasse64)

        classes.add(classe19)


        val classe20 = Classe(20, "Corrosão/irritação à pele")

        val subclasse65 = Subclasse(65, "1A", Texto.PERIGO, mutableListOf(Pictograma.CORROSAO))

        subclasse65.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H314, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P301 + " " + Texto.P330 + " "  + Texto.P331, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P303 + " " + Texto.P361 + " " + Texto.P353, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P363, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P305 + " " + Texto.P351 + " "  + Texto.P338, TipoTexto.RESPOSTA)
            )
        )

        classe20.subclasses.add(subclasse65)

        val subclasse66 = Subclasse(66, "1B", Texto.PERIGO, mutableListOf(Pictograma.CORROSAO))

        subclasse66.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H314, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P301 + " " + Texto.P330 + " "  + Texto.P331, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P303 + " " + Texto.P361 + " " + Texto.P353, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P363, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P305 + " " + Texto.P351 + " "  + Texto.P338, TipoTexto.RESPOSTA)
            )
        )

        classe20.subclasses.add(subclasse66)

        val subclasse67 = Subclasse(67, "1C", Texto.PERIGO, mutableListOf(Pictograma.CORROSAO))

        subclasse67.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H314, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P301 + " " + Texto.P330 + " "  + Texto.P331, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P303 + " " + Texto.P361 + " " + Texto.P353, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P363, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P305 + " " + Texto.P351 + " "  + Texto.P338, TipoTexto.RESPOSTA)
            )
        )

        classe20.subclasses.add(subclasse67)

        val subclasse68 = Subclasse(68, "2", Texto.ATENCAO, mutableListOf(Pictograma.PONTO_EXCLAMACAO))

        subclasse68.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H315, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P302 + " " + Texto.P352, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P332 + " " + Texto.P313, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P362 + " " + Texto.P364, TipoTexto.RESPOSTA)
            )
        )

        classe20.subclasses.add(subclasse68)

        val subclasse69 = Subclasse(69, "5", Texto.ATENCAO, null)

        subclasse69.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H316, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P332 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe20.subclasses.add(subclasse69)

        classes.add(classe20)


        val classe21 = Classe(21, "Lesões oculares graves/irritação ocular")

        val subclasse70 = Subclasse(70, "1", Texto.PERIGO, mutableListOf(Pictograma.CORROSAO))

        subclasse70.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H318, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P305 + " " + Texto.P351 + " " + Texto.P338, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P310, TipoTexto.RESPOSTA)
            )
        )

        classe21.subclasses.add(subclasse70)

        val subclasse71 = Subclasse(71, "2A", Texto.ATENCAO, mutableListOf(Pictograma.PONTO_EXCLAMACAO))

        subclasse71.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H319, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P305 + " " + Texto.P351 + " " + Texto.P338, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P337 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe21.subclasses.add(subclasse71)

        val subclasse72 = Subclasse(72, "2B", Texto.ATENCAO, null)

        subclasse72.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H320, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P305 + " " + Texto.P351 + " " + Texto.P338, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P337 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe21.subclasses.add(subclasse72)

        classes.add(classe21)


        val classe22 = Classe(22, "Sensibilização respiratória")

        val subclasse73 = Subclasse(
            73,
            "1 e subcategorias 1A e 1B",
            Texto.PERIGO,
            mutableListOf(Pictograma.PERIGOSO_SAUDE)
        )

        subclasse73.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H334, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P261, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P284, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P342 + " " + Texto.P311, TipoTexto.RESPOSTA)
            )
        )

        classe22.subclasses.add(subclasse73)

        classes.add(classe22)


        val classe23 = Classe(23, "Sensibilização à pele")

        val subclasse74 = Subclasse(
            74, "1 e subcategorias 1A e 1B", Texto.ATENCAO, mutableListOf(
                Pictograma.PONTO_EXCLAMACAO
            )
        )

        subclasse74.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H317, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P261, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P272, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P302 + " " + Texto.P352, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P333 + " " + Texto.P313, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P321, TipoTexto.RESPOSTA),
                FraseAdvertencia(Texto.P362 + " " + Texto.P364, TipoTexto.RESPOSTA)
            )
        )

        classe23.subclasses.add(subclasse74)

        classes.add(classe23)


        val classe24 = Classe(24, "Mutagenicidade em células germinativas")

        val subclasse75 = Subclasse(75, "1A", Texto.PERIGO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse75.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H340, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe24.subclasses.add(subclasse75)

        val subclasse76 = Subclasse(76, "1B", Texto.PERIGO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse76.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H340, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe24.subclasses.add(subclasse76)

        val subclasse77 = Subclasse(77, "2", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse77.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H341, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe24.subclasses.add(subclasse77)

        classes.add(classe24)


        val classe25 = Classe(25, "Carcinogenicidade")

        val subclasse78 = Subclasse(78, "1A", Texto.PERIGO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse78.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H350, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe25.subclasses.add(subclasse78)

        val subclasse79 = Subclasse(79, "1B", Texto.PERIGO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse79.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H350, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe25.subclasses.add(subclasse79)

        val subclasse80 = Subclasse(80, "2", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse80.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H351, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe25.subclasses.add(subclasse80)

        classes.add(classe25)


        val classe26 = Classe(26, "Toxicidade à reprodução")

        val subclasse81 = Subclasse(81, "1A", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse81.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H360, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe26.subclasses.add(subclasse81)

        val subclasse82 = Subclasse(82, "1B", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse82.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H360, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe26.subclasses.add(subclasse82)

        val subclasse83 = Subclasse(83, "2", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse83.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H361, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P202, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P280, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe26.subclasses.add(subclasse83)

        val subclasse84 = Subclasse(
            84,
            "Categoria adicional para efeitos sobre ou via lactação",
            null,
            null
        )

        subclasse84.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H362, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P201, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P263, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P313, TipoTexto.RESPOSTA)
            )
        )

        classe26.subclasses.add(subclasse84)

        classes.add(classe26)


        val classe27 = Classe(27, "Toxicidade para órgãos-alvo específicos – Exposição única")

        val subclasse85 = Subclasse(85, "1", Texto.PERIGO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse85.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H370, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P311 + " " + Texto.P321, TipoTexto.RESPOSTA)
            )
        )

        classe27.subclasses.add(subclasse85)

        val subclasse86 = Subclasse(86, "2", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse86.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H371, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P308 + " " + Texto.P311, TipoTexto.RESPOSTA)
            )
        )

        classe27.subclasses.add(subclasse86)

        val subclasse87 = Subclasse(87, "3", Texto.ATENCAO, mutableListOf(Pictograma.PONTO_EXCLAMACAO))

        subclasse87.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H335 + "/" + Texto.H336, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P261, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P271, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P304 + " " + Texto.P340 + " " + Texto.P312, TipoTexto.RESPOSTA)
            )
        )

        classe27.subclasses.add(subclasse87)

        classes.add(classe27)


        val classe28 = Classe(28, "Toxicidade para órgãos-alvo específicos – Exposição repetida")

        val subclasse88 = Subclasse(88, "1", Texto.PERIGO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse88.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H372, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P264, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P270, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P314, TipoTexto.RESPOSTA)
            )
        )

        classe28.subclasses.add(subclasse88)

        val subclasse89 = Subclasse(89, "2", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse89.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H373, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P260, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P314, TipoTexto.RESPOSTA)
            )
        )

        classe28.subclasses.add(subclasse89)

        classes.add(classe28)


        val classe29 = Classe(29, "Perigo por aspiração")

        val subclasse90 = Subclasse(90, "1", Texto.PERIGO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse90.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H304, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P311 + " " + Texto.P310 + " " + Texto.P331, TipoTexto.RESPOSTA)
            )
        )

        classe29.subclasses.add(subclasse90)

        val subclasse91 = Subclasse(91, "2", Texto.ATENCAO, mutableListOf(Pictograma.PERIGOSO_SAUDE))

        subclasse91.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H305, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P311 + " " + Texto.P310 + " " + Texto.P331, TipoTexto.RESPOSTA)
            )
        )

        classe29.subclasses.add(subclasse91)

        classes.add(classe29)


        val classe30 = Classe(30, "Perigoso ao ambiente aquático – Agudo")

        val subclasse92 = Subclasse(92, "1", Texto.ATENCAO, mutableListOf(Pictograma.MEIO_AMBIENTE))

        subclasse92.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H400, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P273, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P391, TipoTexto.RESPOSTA)
            )
        )

        classe30.subclasses.add(subclasse92)

        val subclasse93 = Subclasse(93, "2", null, null)

        subclasse93.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H401, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P273, TipoTexto.PREVENCAO)
            )
        )

        classe30.subclasses.add(subclasse93)

        val subclasse94 = Subclasse(94, "3", null, null)

        subclasse94.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H402, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P273, TipoTexto.PREVENCAO)
            )
        )

        classe30.subclasses.add(subclasse94)

        classes.add(classe30)


        val classe31 = Classe(31, "Perigoso ao ambiente aquático – Crônico")

        val subclasse95 = Subclasse(95, "1", Texto.ATENCAO, mutableListOf(Pictograma.MEIO_AMBIENTE))

        subclasse95.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H410, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P273, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P391, TipoTexto.RESPOSTA)
            )
        )

        classe31.subclasses.add(subclasse95)

        val subclasse96 = Subclasse(96, "2", null, mutableListOf(Pictograma.MEIO_AMBIENTE))

        subclasse96.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H411, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P273, TipoTexto.PREVENCAO),
                FraseAdvertencia(Texto.P391, TipoTexto.RESPOSTA)
            )
        )

        classe31.subclasses.add(subclasse96)

        val subclasse97 = Subclasse(97, "3", null, null)

        subclasse97.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H412, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P273, TipoTexto.PREVENCAO)
            )
        )

        classe31.subclasses.add(subclasse97)

        val subclasse98 = Subclasse(98, "4", null, null)

        subclasse98.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H412, TipoTexto.PERIGO),
                FraseAdvertencia(Texto.P273, TipoTexto.PREVENCAO)
            )
        )

        classe31.subclasses.add(subclasse98)

        classes.add(classe31)


        val classe32 = Classe(32, "Perigoso à camada de ozônio")

        val subclasse99 = Subclasse(99, "1", Texto.ATENCAO, mutableListOf(Pictograma.PONTO_EXCLAMACAO))

        subclasse99.frases.addAll(
            mutableListOf<FraseAdvertencia>(
                FraseAdvertencia(Texto.H420, TipoTexto.PERIGO)
            )
        )

        classe32.subclasses.add(subclasse99)

        classes.add(classe32)
    }
}