package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.Subclasse

class SubclasseAdapter(context: Context) : ArrayAdapter<Subclasse>(context,0) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View

        if(convertView != null){
            v = convertView
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.item_lista_subclasse,parent,false)
        }

        val item = getItem(position)

        val txtSubclasse = v.findViewById<TextView>(R.id.txtSubclasse)
        txtSubclasse.text = item?.descricao

        return v
    }

}