package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.Fornecedor

class FornecedorAdapter(context: Context) : ArrayAdapter<Fornecedor>(context,0) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View

        if(convertView != null){
            v = convertView
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.item_lista_fornecedor,parent,false)
        }

        val item = getItem(position)

        val txtFornecedor = v.findViewById<TextView>(R.id.txtFornecedor)
        txtFornecedor.text = item?.nome

        return v
    }

}