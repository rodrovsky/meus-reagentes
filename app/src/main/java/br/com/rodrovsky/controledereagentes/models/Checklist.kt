package br.com.rodrovsky.controledereagentes.models

import java.io.Serializable

data class Checklist(
    val id: Int,
    val data: String,
    val codigo: String,
    val supervisor: String,
    val resposta1: Boolean,
    val resposta2: Boolean,
    val resposta3: Boolean,
    val resposta4: Boolean,
    val resposta5: Boolean,
    val comentarios: String
) : Serializable