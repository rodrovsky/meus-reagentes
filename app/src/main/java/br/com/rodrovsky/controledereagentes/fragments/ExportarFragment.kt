package br.com.rodrovsky.controledereagentes.fragments

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.content.PermissionChecker
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_exportar.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileWriter

class ExportarFragment : Fragment() {
    val PERMISSAO_ESCRITA_ARQUIVO = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exportar, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onStart() {
        super.onStart()

        btn_exportar.setOnClickListener {
            if(ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PermissionChecker.PERMISSION_GRANTED){

                gerarArquivoExportacao()

            } else{
                requestPermissions(
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    PERMISSAO_ESCRITA_ARQUIVO)
            }
        }


    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            PERMISSAO_ESCRITA_ARQUIVO -> {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    gerarArquivoExportacao()
                } else {
                    Toast.makeText(requireContext(),"Sem permissao",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun gerarArquivoExportacao(){

        val activity = requireActivity() as MainActivity

        val lotes = activity.getDadosExportacao()

        var csvHeader = "fornecedor;reagente;lote;data_entrada;data_validade;quantidade_entrada;"
            csvHeader += "data_consumo;quantidade_prevista;quantidade_utilizada "

        val csvPath = File(requireContext().filesDir,"csv")
        if(!csvPath.exists())
            csvPath.mkdir()

        val arquivoCsv = File(csvPath,"consumo-reagentes.csv")

        arquivoCsv.printWriter().use {
            it.println(csvHeader)
            for(lote in lotes){
                if(lote.consumo.size > 0) {
                    // imprime lote + consumo
                    for (consumo in lote.consumo) {
                        it.println(
                            "\"" + lote.fornecedor?.nome + "\";"
                            + "\"" + lote.produto?.nome + "\";"
                            + "\"" + lote.lote + "\";"
                            + "\"" + lote.dataEntrada + "\";"
                            + "\"" + lote.dataValidade + "\";"
                            + lote.quantidade + ";"
                            + "\"" + consumo.data + "\";"
                            + consumo.quantidadePrevista + ";"
                            + consumo.quantidade
                        )
                    }
                } else{
                    // imprime so dados do lote
                    it.println(
                        "\"" + lote.fornecedor?.nome + "\";"
                        + "\"" + lote.produto?.nome + "\";"
                        + "\"" + lote.lote + "\";"
                        + "\"" + lote.dataEntrada + "\";"
                        + "\"" + lote.dataValidade + "\";"
                        + lote.quantidade + ";"
                        + ";"
                        + ";"
                    )
                }

            }
        }

        if(lotes.size > 0) {

            // compartilhar
            val shareIntent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_STREAM,
                    FileProvider.getUriForFile(
                        requireContext(),
                        "br.com.rodrovsky.controledereagentes.fileprovider",
                        arquivoCsv
                    )
                )
                type = "text/csv"
            }

            Intent.createChooser(shareIntent, null)
            startActivity(shareIntent)
        } else{
            Toast.makeText(requireContext(),"Sem dados de lotes cadastrados",Toast.LENGTH_SHORT).show()
        }

    }

}