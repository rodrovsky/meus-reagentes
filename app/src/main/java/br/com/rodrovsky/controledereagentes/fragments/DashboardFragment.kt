package br.com.rodrovsky.controledereagentes.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.MainActivity
import br.com.rodrovsky.controledereagentes.activities.ReagenteActivity
import br.com.rodrovsky.controledereagentes.adapters.ProdutoAdapter
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Produto
import br.com.rodrovsky.controledereagentes.models.User
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.db.SqlOrderDirection
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select

class DashboardFragment : Fragment() {
    lateinit var activity: MainActivity
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onStart() {
        super.onStart()

        activity = requireActivity() as MainActivity
        user = activity.getUser()

        saudacao.text = "Olá " + user.nome

        val consumoAdapter = ProdutoAdapter(activity)
        listMaisConsumidos.adapter = consumoAdapter

        listMaisConsumidos.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(activity, ReagenteActivity::class.java)
            val produto = consumoAdapter.getItem(position)

            intent.putExtra("produto", produto)

            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()

        val adapterMaisConsumidos = listMaisConsumidos.adapter as ProdutoAdapter
        val maisConsumidos = getMaisConsumidos()
        adapterMaisConsumidos.clear()
        adapterMaisConsumidos.addAll(maisConsumidos)

    }

    fun getMaisConsumidos() : List<Produto> {
        activity.apply {
            return database.use {
                val tables = "Produto AS p INNER JOIN Lote AS l ON (l.produto_id = p.id) " +
                        "INNER JOIN Dispersao  AS d ON (d.lote_id = l.id)"
                return@use select(tables)
                    .columns("p.id","p.nome", "p.codigo", "p.qtde_minima", "sum(d.quantidade) AS consumo")
                    .groupBy("p.id, p.nome, p.codigo, p.qtde_minima")
                    .orderBy("consumo", SqlOrderDirection.DESC)
                    .limit(5)
                    .exec {
                        val parser = rowParser {
                            id: Int,
                            nome: String,
                            codigo: String,
                            qtdeMinima: Double,
                            consumo: Double
                                ->
                            Produto(id, nome, codigo, qtdeMinima)
                        }

                        val produtos = parseList(parser)

                        return@exec produtos
                    }
            }
        }
    }
}