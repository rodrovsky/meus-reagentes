package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.User

class UserAdapter(context: Context) : ArrayAdapter<User>(context,0) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View

        if(convertView != null){
            v = convertView
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.item_lista_user,parent,false)
        }

        val item = getItem(position)

        val txtUser = v.findViewById<TextView>(R.id.txtUser)
        txtUser.text = item?.nome

        return v
    }

}