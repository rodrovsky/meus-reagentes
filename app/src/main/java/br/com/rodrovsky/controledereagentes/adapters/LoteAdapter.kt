package br.com.rodrovsky.controledereagentes.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.Lote
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class LoteAdapter(context: Context) : ArrayAdapter<Lote>(context, 0) {
    val df = DecimalFormat("#,###.00")
    val sdf = SimpleDateFormat("dd/MM/yyyy")
    val limiteVencimento = Calendar.getInstance()
    val dataAtual = Calendar.getInstance()

    @SuppressLint("ResourceAsColor")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View

        if(convertView!=null){
            v = convertView
        }else{
            v = LayoutInflater.from(context).inflate(R.layout.item_lista_lote,parent,false)
        }

        val item = getItem(position)

        val txtFornecedor =  v.findViewById<TextView>(R.id.txtFornecedor)
        txtFornecedor.text = item?.fornecedor?.nome

        val txtLote =  v.findViewById<TextView>(R.id.txtLote)
        txtLote.text = item?.lote

        val txtQuantidade =  v.findViewById<TextView>(R.id.txtQuantidade)
        txtQuantidade.text = df.format(item?.quantidadeAtual)

        // rotular segundo data de vencimento
        limiteVencimento.time = dataAtual.time
        limiteVencimento.add(Calendar.DATE, 90)
        val vencimento = sdf.parse(item?.dataValidade)

        if(vencimento.before(limiteVencimento.time)){
            txtFornecedor.setTextColor(Color.parseColor("#FF8607"))
            txtLote.setTextColor(Color.parseColor("#FF8607"))
            txtQuantidade.setTextColor(Color.parseColor("#FF8607"))
        }

        if(vencimento.before(dataAtual.time)){
            txtFornecedor.setTextColor(Color.RED)
            txtLote.setTextColor(Color.RED)
            txtQuantidade.setTextColor(Color.RED)
        }

        return v
    }

}