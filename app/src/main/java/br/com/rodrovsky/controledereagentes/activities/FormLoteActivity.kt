package br.com.rodrovsky.controledereagentes.activities

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.adapters.FornecedorAdapter
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Dispersao
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import br.com.rodrovsky.controledereagentes.models.Lote
import br.com.rodrovsky.controledereagentes.models.Produto
import br.com.rodrovsky.controledereagentes.util.DateWatcher
import br.com.rodrovsky.controledereagentes.util.toEditable
import kotlinx.android.synthetic.main.activity_dispersao_lote.*
import kotlinx.android.synthetic.main.activity_form_dispersao.*
import kotlinx.android.synthetic.main.activity_form_lote.*
import kotlinx.android.synthetic.main.activity_form_lote.txtLoteDataEntrada
import kotlinx.android.synthetic.main.activity_form_lote.txtLoteDataValidade
import kotlinx.android.synthetic.main.activity_form_lote.txtLoteLote
import kotlinx.android.synthetic.main.activity_form_lote.txtLoteQuantidade
import kotlinx.android.synthetic.main.fragment_fornecedor.*
import kotlinx.android.synthetic.main.item_lista_lote.*
import org.jetbrains.anko.db.*
import org.jetbrains.anko.toast
import java.text.DecimalFormat
import java.util.*

class FormLoteActivity : AppCompatActivity() {
    val df = DecimalFormat("#,###.00")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_lote)

        val adapter = FornecedorAdapter(this)
        val fornecedores = listaFornecedor()
        spnLoteFornecedor.adapter = adapter
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter.addAll(fornecedores)

        val paramCadastro = intent?.extras?.getSerializable("produto")
        val paramAlteracao = intent?.extras?.getSerializable("lote")

        val c = Calendar.getInstance()
        val ano = c.get(Calendar.YEAR)
        val mes = c.get(Calendar.MONTH)
        val dia = c.get(Calendar.DAY_OF_MONTH)

        btnLoteDataEntrada.setOnClickListener{
            val dpDataEntrada = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                val strDay = if(day<10) ("0" + day.toString()) else day
                val strMonth = if(month<9) ("0" + (month + 1).toString()) else month + 1
                txtLoteDataEntrada.text = ("" + strDay + "/" + strMonth + "/" + year).toEditable()
            }, ano, mes, dia)

            dpDataEntrada.show()
        }

        btnLoteDataValidade.setOnClickListener {
            val dpDataValidade = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                val strDay = if(day<10) ("0" + day.toString()) else day
                val strMonth = if(month<9) ("0" + (month + 1).toString()) else (month + 1)
                txtLoteDataValidade.text = ("" + strDay + "/" + strMonth + "/" + year).toEditable()
            }, ano, mes, dia)

            dpDataValidade.show()
        }

        txtLoteDataEntrada.addTextChangedListener(DateWatcher(txtLoteDataEntrada))
        txtLoteDataValidade.addTextChangedListener(DateWatcher(txtLoteDataValidade))

        // cadastro
        if(paramCadastro != null){
            val produto = paramCadastro as Produto

            btn_salvar_lote.setOnClickListener {
                if(validarDadosCadastro()){
                    val fornecedor = spnLoteFornecedor.selectedItem as Fornecedor
                    val lote = txtLoteLote.text.toString()
                    val dataEntrada = txtLoteDataEntrada.text.toString()
                    val dataValidade = txtLoteDataValidade.text.toString()
                    val quantidade = txtLoteQuantidade.text.toString()

                    database.use {
                        val id = insert("Lote",
                            "lote" to lote,
                            "data_entrada" to dataEntrada,
                            "data_validade" to dataValidade,
                            "quantidade" to df.parse(quantidade).toString(),
                            "quantidade_atual" to df.parse(quantidade).toString(),
                            "produto_id" to produto.id,
                            "fornecedor_id" to fornecedor.id
                        )

                        if(id !== -1L){
                            // limparCampos()
                            toast("Cadastrado com sucesso")
                            finish()
                        } else{
                            toast("Ocorreu um erro")
                        }
                    }
                }
            }
        }

        if(paramAlteracao != null){
            val lote = paramAlteracao as Lote

            labelFormLote.text = "Editar Lote de Reagente"
            val fornecedorPos = adapter.getPosition(Fornecedor(lote.fornecedor!!.id,lote.fornecedor!!.nome))
            spnLoteFornecedor.setSelection(fornecedorPos)
            txtLoteLote.text = lote.lote.toEditable()
            txtLoteDataEntrada.text = lote.dataEntrada.toEditable()
            txtLoteDataValidade.text = lote.dataValidade.toEditable()
            txtLoteQuantidade.text = df.format(lote.quantidade).toEditable()
            btn_salvar_lote.text = "Salvar Alterações"

            btn_salvar_lote.setOnClickListener {
                if(validarDadosCadastro()){
                    val qtde = df.parse(txtLoteQuantidade.text.toString()).toDouble()
                    val dispersoesLote = listaDispersao(lote)
                    val totalDispersoes = dispersoesLote.sumByDouble { it.quantidade }
                    val qtdeAtual = qtde - totalDispersoes

                    if(qtde < totalDispersoes){
                        txtLoteQuantidade.error = "A Quantidade não pode ser menor que a soma das dispersões registradas"
                    } else {


                        database.use {
                            val rs = update(
                                "Lote",
                                "fornecedor_id" to (spnLoteFornecedor.selectedItem as Fornecedor).id,
                                "lote" to txtLoteLote.text.toString(),
                                "data_entrada" to txtLoteDataEntrada.text.toString(),
                                "data_validade" to txtLoteDataValidade.text.toString(),
                                "quantidade" to qtde.toString(),
                                "quantidade_atual" to qtdeAtual.toString()
                            ).whereArgs("id = {id}", "id" to lote.id)
                                .exec()

                            if (rs > 0) {
                                toast("Atualizado com sucesso")
                                finish()
                            } else {
                                toast("Ocorreu um erro")
                            }

                        }
                    }
                }
            }
        }
    }

    fun listaFornecedor() : List<Fornecedor>{
        return database.use {
            return@use select("Fornecedor")
                .columns("id","nome")
                .exec {
                    val parser = rowParser {
                            id: Int,
                            nome: String ->
                        Fornecedor(id, nome)
                    }

                    var fornecedores = parseList(parser)
                    return@exec fornecedores
                }
        }
    }

    fun listaDispersao(lote: Lote) : List<Dispersao>{
        return database.use {
            return@use select("Dispersao")
                .columns("id", "quantidade")
                .whereArgs("lote_id = {loteId}", "loteId" to lote.id)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            quantidade: Double ->
                        Dispersao(id,quantidade)

                    }

                    val registros = parseList(parser)

                    return@exec registros
                }
        }
    }

    fun validarDadosCadastro() : Boolean{
        if(txtLoteLote.text.isEmpty()){
            txtLoteLote.error = "Informe o lote"
            return false
        }

        if(txtLoteDataEntrada.text.isEmpty()
            || txtLoteDataEntrada.text.contains("D")
            || txtLoteDataEntrada.text.contains("M")
            || txtLoteDataEntrada.text.contains("A")){
            txtLoteLote.error = "Informe a data de entrada"
            return false
        }

        if(txtLoteDataValidade.text.isEmpty()
            || txtLoteDataValidade.text.contains("D")
            || txtLoteDataValidade.text.contains("M")
            || txtLoteDataValidade.text.contains("A")){
            txtLoteLote.error = "Informe a data de validade"
            return false
        }

        if(txtLoteQuantidade.text.isEmpty()){
            txtLoteLote.error = "Informe a quantidade"
            return false
        }

        return true
    }


}