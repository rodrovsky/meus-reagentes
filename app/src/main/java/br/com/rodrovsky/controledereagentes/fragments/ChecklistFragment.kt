package br.com.rodrovsky.controledereagentes.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.FormChecklistActivity
import br.com.rodrovsky.controledereagentes.activities.FormProdutoActivity
import br.com.rodrovsky.controledereagentes.activities.MainActivity
import br.com.rodrovsky.controledereagentes.activities.VerChecklistActivity
import br.com.rodrovsky.controledereagentes.adapters.ChecklistAdapter
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.Checklist
import br.com.rodrovsky.controledereagentes.models.Produto
import kotlinx.android.synthetic.main.fragment_checklist.*
import kotlinx.android.synthetic.main.fragment_produto.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class ChecklistFragment : Fragment() {
    lateinit var activity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_checklist, container, false)
    }

    override fun onStart() {
        super.onStart()

        val adapter = ChecklistAdapter(requireContext())
        listaChecklist.adapter = adapter

        registerForContextMenu(listaChecklist)

        activity = requireActivity() as MainActivity

        txtPesquisaChecklist.addTextChangedListener( object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                atualizaListaCheckList(s.toString())
            }

            override fun afterTextChanged(s: Editable?){}

        })

        fab_add_checklist.setOnClickListener {
            val intent = Intent(activity, FormChecklistActivity::class.java)
            startActivity(intent)
        }

        listaChecklist.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(context, VerChecklistActivity::class.java)
            val checklist = adapter.getItem(position)
            intent.putExtra("checklist",checklist)

            startActivity(intent)
        }

        atualizaListaCheckList("")

    }

    override fun onResume() {
        super.onResume()

        atualizaListaCheckList(txtPesquisaChecklist.text.toString())
    }

    fun atualizaListaCheckList(pesquisa: String){
        val lista = listaChecklist(pesquisa)

        val adapter = listaChecklist.adapter as ChecklistAdapter

        adapter.clear()
        adapter.addAll(lista)

    }

    fun listaChecklist(pesquisa: String) : List<Checklist>{

        val pesquisaAux = "%${pesquisa}%"

        return activity.run {
            database.use {
                return@use select("Checklist")
                    .whereArgs("data like {pesquisa} or supervisor like {pesquisa}",
                        "pesquisa" to pesquisaAux)
                    .exec {
                        val parser = rowParser {
                            id: Int,
                            data: String,
                            codigo: String,
                            supervisor: String,
                            resposta1: Int,
                            resposta2: Int,
                            resposta3: Int,
                            resposta4: Int,
                            resposta5: Int,
                            comentarios: String
                            ->
                            Checklist(
                                id, data, codigo, supervisor,
                                if(resposta1 == 0) false else true,
                                if(resposta2 == 0) false else true,
                                if(resposta3 == 0) false else true,
                                if(resposta4 == 0) false else true,
                                if(resposta5 == 0) false else true,
                                comentarios
                            )
                        }

                        return@exec parseList(parser)
                    }
            }
        }
    }

    fun deleteChecklist(id : Int) {
        activity.apply {
            database.use {
                delete("Checklist", "id = {id}", "id" to id)
            }
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        activity.menuInflater.inflate(R.menu.list_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {

        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val checklist = listaChecklist.getItemAtPosition(info.position) as Checklist

        when(item.itemId){
            R.id.editar_item -> {
                val intent = Intent(activity.applicationContext, FormChecklistActivity::class.java)
                intent.putExtra("checklist",checklist)
                startActivity(intent)
            }

            R.id.excluir_item -> {
                activity.apply {
                    alert("excluir checklist?", "Atenção") {
                        yesButton {
                            this@ChecklistFragment.deleteChecklist(checklist.id)
                            toast("Excluído com sucesso")
                            this@ChecklistFragment.onResume()
                        }

                        noButton {

                        }
                    }.show()
                }
            }
        }

        return super.onContextItemSelected(item)
    }

}