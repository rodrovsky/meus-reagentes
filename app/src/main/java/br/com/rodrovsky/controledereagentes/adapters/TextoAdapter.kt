package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.FraseAdvertencia
import kotlinx.android.synthetic.main.item_lista_texto.view.*

class TextoAdapter(val textos: List<String>, val context: Context) : Adapter<TextoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_lista_texto, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val texto = textos[position]
        holder.texto.text = texto
    }

    override fun getItemCount(): Int {
        return textos.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val texto = itemView.txtTexto
    }
}