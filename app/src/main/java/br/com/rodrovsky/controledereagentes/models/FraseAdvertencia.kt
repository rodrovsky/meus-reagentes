package br.com.rodrovsky.controledereagentes.models

data class FraseAdvertencia(val texto: String, val tipo: Int)