package br.com.rodrovsky.controledereagentes.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import br.com.rodrovsky.controledereagentes.adapters.FornecedorAdapter
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.FormFornecedorActivity
import br.com.rodrovsky.controledereagentes.activities.MainActivity
import br.com.rodrovsky.controledereagentes.activities.VerFornecedorActivity
import br.com.rodrovsky.controledereagentes.models.Fornecedor
import br.com.rodrovsky.controledereagentes.models.Lote
import kotlinx.android.synthetic.main.activity_lote_produto.*
import kotlinx.android.synthetic.main.fragment_fornecedor.*


class FornecedorFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fornecedor, container, false)
    }

    override fun onStart() {
        super.onStart()

        val adapter =
            FornecedorAdapter(
                activity?.applicationContext!!
            )
        list_fornecedor.adapter = adapter
        registerForContextMenu(list_fornecedor)

        fab_add_fornecedor.setOnClickListener { view ->
            val intent = Intent(context,FormFornecedorActivity::class.java)
            startActivity(intent)
        }

        list_fornecedor.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(context,VerFornecedorActivity::class.java)
            val fornecedor = adapter.getItem(position)
            intent.putExtra("fornecedor",fornecedor)
            startActivity(intent)
        }

        txtPesquisaFornecedor.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s != null) {
                    pesquisar(s.toString())
                }
            }

        })
    }

    override fun onResume() {
        super.onResume()
        pesquisar("")


    }

    fun pesquisar(pesquisa: String){
        val adapter = list_fornecedor.adapter as FornecedorAdapter
        val fornecedores = (activity as MainActivity).pesquisarFornecedor(pesquisa)

        adapter.clear()
        adapter.addAll(fornecedores)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        activity?.menuInflater?.inflate(R.menu.list_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val fornecedor = list_fornecedor.getItemAtPosition(info.position) as Fornecedor

        when(item.itemId){
            R.id.editar_item -> {
                val intent = Intent(requireActivity().applicationContext, FormFornecedorActivity::class.java)
                intent.putExtra("fornecedor",fornecedor)
                startActivity(intent)
            }

            R.id.excluir_item -> {
                (requireActivity() as MainActivity).confirmDeleteFornecedor(fornecedor.id, this)
            }
        }

        return super.onContextItemSelected(item)
    }

}