package br.com.rodrovsky.controledereagentes.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.*
import br.com.rodrovsky.controledereagentes.util.classes
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.db.parseOpt
import org.jetbrains.anko.db.parseSingle
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast
import java.util.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_primeiro_acesso.setOnClickListener {
            val intent = Intent(this, FormUserActivity::class.java)
            startActivity(intent)
        }

        btn_entrar.setOnClickListener {
            if(validarForm()){
                val usuario = login.text.toString()
                val senha = senha.text.toString()

                val user = login(usuario,senha)

                if(user != null){
                    limpaForm()
                    val intent = Intent(this,MainActivity::class.java)
                    intent.putExtra("user",user)
                    startActivity(intent)
                } else {
                    login.error = "Usuário e/ou senha incorreto(s)"
                }
            }
        }
    }

    fun validarForm() : Boolean{
        if(login.text.isEmpty()){
            login.error = "Informe o usuário"
            return false
        }

        if(senha.text.isEmpty()){
            senha.error = "Informe a senha"
            return false
        }

        return true
    }

    override fun onResume() {
        super.onResume()

        if(getNumUsers() > 0){
            btn_entrar.isEnabled = true
            btn_primeiro_acesso.visibility = View.INVISIBLE
            login.isEnabled = true
            senha.isEnabled = true
        } else {
            btn_entrar.isEnabled = false
            btn_primeiro_acesso.visibility = View.VISIBLE
            login.isEnabled = false
            senha.isEnabled = false
        }
    }

    fun getNumUsers() : Int{
        return database.use {
            return@use select("Usuario")
                .columns("count(*)")
                .exec {
                    val parser = rowParser {
                        users: Int ->
                        users
                    }

                    val users = parseSingle(parser)

                    return@exec users
                }
        }
    }

    fun login(user: String, senha: String) : User? {
        return database.use {
            return@use select("Usuario")
                .columns("id","nome","email","telefone","matricula","senha","perfil_id")
                .whereArgs("matricula = {login} and senha = {senha}",
                    "login" to user, "senha" to senha)
                .exec {
                    val parser = rowParser {
                            id: Int,
                            nome: String,
                            email: String,
                            telefone: String,
                            matricula: String,
                            senha: String,
                            perfilTmp: String?
                        ->
                        val perfil = if(perfilTmp == null) User.ADMINISTRADOR else perfilTmp.toInt()
                        User(id,nome, matricula, email, telefone, senha, perfil)
                    }

                    val user = parseOpt(parser)
                    return@exec user
                }
        }
    }

    fun limpaForm(){
        senha.text.clear()
        login.text.clear()
    }
}