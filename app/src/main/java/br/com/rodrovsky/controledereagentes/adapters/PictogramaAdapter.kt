package br.com.rodrovsky.controledereagentes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.models.Pictograma
import kotlinx.android.synthetic.main.item_lista_pictograma.view.*

class PictogramaAdapter(val pictogramas : List<Int>, val context : Context): RecyclerView.Adapter<PictogramaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_lista_pictograma, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pictograma = pictogramas[position]
        holder.pictograma.setImageResource(pictograma)
    }

    override fun getItemCount(): Int {
        return pictogramas.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pictograma = itemView.imgPictograma
    }
}