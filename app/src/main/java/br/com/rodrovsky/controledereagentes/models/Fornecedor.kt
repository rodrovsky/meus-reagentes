package br.com.rodrovsky.controledereagentes.models

import java.io.Serializable


data class Fornecedor(val id: Int, val nome: String) : Serializable{
    var endereco: String? = null
    var email: String? = null
    var telefone: String? = null
    var telefoneEmergencia: String? = null

    constructor(id: Int, nome: String,
                endereco: String,
                email: String,
                telefone: String,
                telefoneEmergencia: String
        ) : this(id, nome) {

        this.endereco = endereco
        this.email = email
        this.telefone = telefone
        this.telefoneEmergencia = telefoneEmergencia
    }

    override fun toString(): String {
        return this.nome
    }
}