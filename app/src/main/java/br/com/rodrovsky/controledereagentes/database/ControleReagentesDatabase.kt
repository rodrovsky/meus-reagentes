package br.com.rodrovsky.controledereagentes.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

val Context.database: ControleReagentesDatabase
    get() = ControleReagentesDatabase.getInstance(
        applicationContext
    )

class ControleReagentesDatabase(context: Context) : ManagedSQLiteOpenHelper(ctx = context, name = "ControleReagentes.db", version = 5){

    companion object {
        private var instance : ControleReagentesDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context) : ControleReagentesDatabase {
            if(instance == null){
                instance =
                    ControleReagentesDatabase(ctx.applicationContext)
            }

            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {


        db.createTable("Fornecedor", true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "nome" to TEXT,
                "endereco" to TEXT,
                "email" to TEXT,
                "telefone" to TEXT,
                "telefone_emergencia" to TEXT
            )

        db.createTable("Produto", true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "nome" to TEXT,
                "codigo" to TEXT,
                "qtde_minima" to REAL,
                "controlado" to INTEGER
            )

        db.createTable("Lote", true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "lote" to TEXT,
                "data_entrada" to TEXT,
                "data_validade" to TEXT,
                "quantidade" to REAL,
                "quantidade_atual" to REAL,
                "produto_id" to INTEGER,
                "fornecedor_id" to INTEGER,
                FOREIGN_KEY("produto_id","Produto","id"),
                FOREIGN_KEY("fornecedor_id","Fornecedor","id")
            )

        db.createTable("Dispersao", true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "data" to TEXT,
                "quantidade_prevista" to REAL,
                "quantidade" to REAL,
                "lote_id" to INTEGER,
                FOREIGN_KEY("lote_id", "Lote", "id")
            )

        db.createTable("ProdutoRisco", true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "produto_id" to INTEGER,
                "classe_id" to INTEGER,
                "subclasse_id" to INTEGER,
                FOREIGN_KEY("produto_id", "Produto","id")
            )

        // tabela de checklist de seguranca
        db.createTable("Checklist", true,
            "id" to INTEGER + PRIMARY_KEY + UNIQUE,
            "data" to TEXT,
            "codigo" to TEXT,
            "supervisor" to TEXT,
            "reposta_1" to INTEGER,
            "reposta_2" to INTEGER,
            "reposta_3" to INTEGER,
            "reposta_4" to INTEGER,
            "reposta_5" to INTEGER,
            "comentarios" to TEXT
        )

        // tabela de usuarios
        db.createTable("Usuario", true,
            "id" to INTEGER + PRIMARY_KEY + UNIQUE,
            "nome" to TEXT,
            "matricula" to TEXT,
            "email" to TEXT,
            "telefone" to TEXT,
            "senha" to TEXT,
            "perfil_id" to TEXT

        )

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        when(oldVersion) {
            1 -> {

                // tabela de checklist de seguranca
                db?.createTable("Checklist", true,
                    "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                    "data" to TEXT,
                    "codigo" to TEXT,
                    "supervisor" to TEXT,
                    "reposta_1" to INTEGER,
                    "reposta_2" to INTEGER,
                    "reposta_3" to INTEGER,
                    "reposta_4" to INTEGER,
                    "reposta_5" to INTEGER,
                    "comentarios" to TEXT
                )
            }

            2 -> {
                // tabela de usuarios
                db.createTable("Usuario", true,
                    "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                    "nome" to TEXT,
                    "matricula" to TEXT,
                    "email" to TEXT,
                    "telefone" to TEXT,
                    "senha" to TEXT
                )
            }

            3 -> {
                db.execSQL("ALTER TABLE Usuario ADD COLUMN perfil_id TEXT")
            }

            4 -> {
                db.execSQL("ALTER TABLE Produto ADD COLUMN controlado INTEGER")
            }
        }
    }

}