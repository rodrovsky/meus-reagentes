package br.com.rodrovsky.controledereagentes.models

data class ProdutoRisco(val id: Int, val classeId : Int, val subclasseId : Int)