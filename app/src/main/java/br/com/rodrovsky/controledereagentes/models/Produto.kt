package br.com.rodrovsky.controledereagentes.models

import java.io.Serializable

data class Produto(val id: Int, val nome: String) : Serializable{
    var codigo: String? = null
    var qtdeMinima: Double = 0.0
    var controlado: Boolean = false
    var vencidos = 0

    constructor(
            id: Int, nome: String,
            codigo: String,
            qtdeMinima: Double
        ) : this(id, nome){

        this.codigo = codigo
        this.qtdeMinima = qtdeMinima
    }

    constructor(
        id: Int, nome: String,
        codigo: String,
        qtdeMinima: Double,
        controlado: Boolean
    ) : this(id, nome, codigo, qtdeMinima){
        this.controlado = controlado
    }
}