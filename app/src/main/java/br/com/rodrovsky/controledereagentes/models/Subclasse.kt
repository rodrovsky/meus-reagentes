package br.com.rodrovsky.controledereagentes.models

import br.com.rodrovsky.controledereagentes.R

data class Subclasse(val id: Int, val descricao: String, val palavra: String?, val pictograma: MutableList<Int>?) {
    val frases = mutableListOf<FraseAdvertencia>()

    override fun toString(): String {
        return this.descricao
    }
}