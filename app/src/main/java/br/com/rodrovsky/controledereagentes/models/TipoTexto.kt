package br.com.rodrovsky.controledereagentes.models

class TipoTexto {
    companion object {
        const val PERIGO = 1
        const val GERAL = 2
        const val PREVENCAO = 3
        const val RESPOSTA = 4
        const val ARMAZENAMENTO = 5
        const val DISPOSICAO = 6
    }
}