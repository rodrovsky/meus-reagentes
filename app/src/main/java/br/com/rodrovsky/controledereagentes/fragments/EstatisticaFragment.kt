package br.com.rodrovsky.controledereagentes.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.ReagenteActivity
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import kotlinx.android.synthetic.main.fragment_estatistica.*
import java.util.*


class EstatisticaFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_estatistica, container, false)
    }

    override fun onStart() {
        super.onStart()

        val reagenteActivity = (requireActivity() as ReagenteActivity)

        reagenteActivity.ocultarInserir()

        val dispersoes = reagenteActivity.getConsumo(reagenteActivity.getProduto(0).id)

        if(dispersoes.size > 0) {

            val dataset = mutableListOf<DataPoint>()
            val datasDispersao = mutableListOf<String>()

            // decomposicao dos dados consolidados para melhor visualizacao
            for((index,dado) in dispersoes.withIndex()){
                dataset.add(DataPoint(index.toDouble(),dado.y))

                val c: Calendar = Calendar.getInstance()
                c.setTimeInMillis(dado.x.toLong())
                val mes: Int = c.get(Calendar.MONTH)
                val dia: Int = c.get(Calendar.DAY_OF_MONTH)
                val dataDispersao = dia.toString() + "/" + (mes + 1).toString()

                datasDispersao.add(dataDispersao)
            }


            val series: BarGraphSeries<DataPoint> = BarGraphSeries<DataPoint>(
                dataset.toTypedArray()
            )

            series.color = Color.parseColor("#B43124")
            series.spacing = 15
            series.setDrawValuesOnTop(true)
            series.setValuesOnTopColor(Color.BLACK)


            dispersaoChart.addSeries(series)

//            dispersaoChart.gridLabelRenderer.setHorizontalLabelsAngle(90)
            dispersaoChart.gridLabelRenderer.numHorizontalLabels = dataset.size
            dispersaoChart.gridLabelRenderer.numVerticalLabels = dataset.size

            dispersaoChart.viewport.isScrollable = true
            dispersaoChart.viewport.isScalable = true


            dispersaoChart.getGridLabelRenderer()
                .setLabelFormatter(object : DateAsXAxisLabelFormatter(context) {
                    override fun formatLabel(value: Double, isValueX: Boolean): String {
                        return if (isValueX) {
                            val datasArray = datasDispersao.toTypedArray()

                            datasArray[value.toInt()]
                        } else {
                            super.formatLabel(value, isValueX)
                        }
                    }
                })
        }
    }
}