package br.com.rodrovsky.controledereagentes.fragments

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.DispersaoLoteActivity
import br.com.rodrovsky.controledereagentes.activities.FormLoteActivity
import br.com.rodrovsky.controledereagentes.activities.MainActivity
import br.com.rodrovsky.controledereagentes.activities.ReagenteActivity
import br.com.rodrovsky.controledereagentes.adapters.LoteAdapter
import br.com.rodrovsky.controledereagentes.models.Lote
import br.com.rodrovsky.controledereagentes.models.Produto
import kotlinx.android.synthetic.main.activity_lote_produto.*
import kotlinx.android.synthetic.main.activity_reagente.*
import kotlinx.android.synthetic.main.fragment_lotes.*
import kotlinx.android.synthetic.main.fragment_lotes.list_lote
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class LotesFragment : Fragment() {
    private lateinit var produto: Produto

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lotes, container, false)
    }

    override fun onStart() {
        super.onStart()

        val reagenteActivity = (requireActivity() as ReagenteActivity)

        reagenteActivity.mostrarInserir()

        this.produto = reagenteActivity.getProduto(0)

        val adapter = LoteAdapter(activity?.applicationContext!!)
        list_lote.adapter = adapter
        registerForContextMenu(list_lote)

        list_lote.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(requireContext(), DispersaoLoteActivity::class.java)

            val lote = adapter.getItem(position)
            intent.putExtra("lote",lote)

            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()

        val adapter = list_lote.adapter as LoteAdapter
        val lotes = (requireActivity() as ReagenteActivity).listaLote(this.produto.id)

        adapter.clear()
        adapter.addAll(lotes)

        (requireActivity() as ReagenteActivity).atualizaTotal()
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        requireActivity().menuInflater.inflate(R.menu.list_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val itemLote = list_lote.getItemAtPosition(info.position) as Lote
        val lote = (requireActivity() as ReagenteActivity).getLote(itemLote.id)

        when(item.itemId) {
            R.id.editar_item -> {
                val intent = Intent(requireContext(), FormLoteActivity::class.java)
                intent.putExtra("lote",lote)
                startActivity(intent)
            }

            R.id.excluir_item -> {
                (requireActivity() as ReagenteActivity).confirmDeleteLote(lote, this)
            }

        }



        return super.onContextItemSelected(item)

    }
}