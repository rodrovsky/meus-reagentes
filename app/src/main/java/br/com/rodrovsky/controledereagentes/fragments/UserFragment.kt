package br.com.rodrovsky.controledereagentes.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import br.com.rodrovsky.controledereagentes.R
import br.com.rodrovsky.controledereagentes.activities.FormUserActivity
import br.com.rodrovsky.controledereagentes.activities.MainActivity
import br.com.rodrovsky.controledereagentes.adapters.UserAdapter
import br.com.rodrovsky.controledereagentes.database.database
import br.com.rodrovsky.controledereagentes.models.User
import kotlinx.android.synthetic.main.fragment_user.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class UserFragment : Fragment() {
    lateinit var activity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onStart() {
        super.onStart()

        activity = requireActivity() as MainActivity

        val adapter = UserAdapter(requireContext())
        list_user.adapter = adapter
        registerForContextMenu(list_user)

        txtPesquisaUser.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                atualizaListaUsers(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {}

        })

        fab_add_user.setOnClickListener {
            val intent = Intent(context, FormUserActivity::class.java)
            startActivity(intent)
        }

//        list_user.setOnItemClickListener { parent, view, position, id ->
//
//        }

        atualizaListaUsers("")
    }

    override fun onResume() {
        super.onResume()

        atualizaListaUsers(txtPesquisaUser.text.toString())
    }

    fun atualizaListaUsers(pesquisa: String) {
        val lista = listaUsers(pesquisa)
        val adapter = list_user.adapter as UserAdapter

        adapter.clear()
        adapter.addAll(lista)
    }

    fun listaUsers(pesquisa: String) : List<User> {
        val aux = "%${pesquisa}%"

        return activity.run {
            database.use {
                return@use select("Usuario")
                    .columns("id","nome","email","telefone","matricula","senha","perfil_id")
                    .whereArgs("matricula like {busca} or nome like {busca}", "busca" to aux)
                    .exec {
                        val parser = rowParser {
                            id: Int,
                            nome: String,
                            email: String,
                            telefone: String,
                            matricula: String,
                            senha: String,
                            perfilTmp: String?
                            ->
                            val perfil = if(perfilTmp == null) User.ADMINISTRADOR else perfilTmp.toInt()
                            User(id,nome, matricula, email, telefone, senha, perfil)
                        }

                        val users = parseList(parser)

                        return@exec users
                    }
            }
        }
    }

    fun deleteUser(id: Int) {
        activity.apply {
            database.use {
                delete("Usuario", "id = {id}", "id" to id)
            }
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        activity.menuInflater.inflate(R.menu.list_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val user = list_user.getItemAtPosition(info.position) as User

        when(item.itemId){
            R.id.editar_item -> {
                activity.apply {
                    val intent = Intent(activity.applicationContext, FormUserActivity::class.java)
                    intent.putExtra("user", user)
                    startActivity(intent)
                }
            }

            R.id.excluir_item -> {
                activity.apply {
                    if(user.id == 1){
                        alert("O administrador primário não pode ser excluído.",
                                "Atenção"
                            ){
                            yesButton {  }
                        }.show()
                    } else {
                        alert("Excluir usuário?", "Atenção") {
                            yesButton {
                                this@UserFragment.deleteUser(user.id)
                                toast("Excluído com sucesso")
                                this@UserFragment.onResume()
                            }
                            noButton {  }
                        }.show()
                    }
                }
            }
        }

        return super.onContextItemSelected(item)
    }
}