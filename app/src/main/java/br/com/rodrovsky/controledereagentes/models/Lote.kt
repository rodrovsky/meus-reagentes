package br.com.rodrovsky.controledereagentes.models

import java.io.Serializable

data class Lote(val id: Int, val lote: String) : Serializable {
    var dataEntrada: String = ""
    var dataValidade: String = ""
    var quantidade: Double = 0.0
    var quantidadeAtual: Double = 0.0
    var produto: Produto? = null
    var fornecedor: Fornecedor? = null
    val consumo = mutableListOf<Dispersao>()

    constructor(id: Int, lote: String, dataEntrada: String,
                dataValidade: String, quantidade: Double,
                quantidadeAtual: Double, produto: Produto,
                fornecedor: Fornecedor) : this(id,lote) {

        this.dataEntrada = dataEntrada
        this.dataValidade = dataValidade
        this.quantidade = quantidade
        this.quantidadeAtual = quantidadeAtual
        this.produto = produto
        this.fornecedor = fornecedor
    }
}

//data class Lote (
//    val id: Int,
//    val lote: String,
//    val dataEntrada: String,
//    val dataValidade: String,
//    val quantidade: Double,
//    val quantidadeAtual: Double,
//    val produto: Produto,
//    val fornecedor: Fornecedor
//) : Serializable